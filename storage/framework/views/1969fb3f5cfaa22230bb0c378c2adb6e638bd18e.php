<?php if($customFields): ?>
<h5 class="col-12 pb-4"><?php echo trans('lang.main_fields'); ?></h5>
<?php endif; ?>
<div style="flex: 50%;max-width: 50%;padding: 0 4px;" class="column">
<!-- Question Field -->
<div class="form-group row ">
  <?php echo Form::label('name', trans("lang.subcategories_question"), ['class' => 'col-3 control-label text-right']); ?>

  <div class="col-9">
    <?php echo Form::text('name', null, ['class' => 'form-control','placeholder'=>
     trans("lang.subcategories_question_placeholder")  ]); ?>

    <div class="form-text text-muted"><?php echo e(trans("lang.subcategories_question_help")); ?></div>
  </div>
</div>

<!-- Answer Field -->
<div class="form-group row ">
  <?php echo Form::label('description', trans("lang.subcategories_answer"), ['class' => 'col-3 control-label text-right']); ?>

  <div class="col-9">
    <?php echo Form::textarea('description', null, ['class' => 'form-control','placeholder'=>
     trans("lang.subcategories_placeholder")  ]); ?>

    <div class="form-text text-muted"><?php echo e(trans("lang.subcategories_answer_help")); ?></div>
  </div>
</div>
</div>
<div style="flex: 50%;max-width: 50%;padding: 0 4px;" class="column">

<!-- Faq Category Id Field -->
<div class="form-group row ">
  <?php echo Form::label('IDcategorie', trans("lang.subcategories_category_id"),['class' => 'col-3 control-label text-right']); ?>

  <div class="col-9">
    <?php echo Form::select('IDcategorie', $category, null, ['class' => 'select2 form-control']); ?>

    <div class="form-text text-muted"><?php echo e(trans("lang.subcategories_category_id_help")); ?></div>
  </div>
</div>

</div>
<?php if($customFields): ?>
<div class="clearfix"></div>
<div class="col-12 custom-field-container">
  <h5 class="col-12 pb-4"><?php echo trans('lang.custom_field_plural'); ?></h5>
  <?php echo $customFields; ?>

</div>
<?php endif; ?>
<!-- Submit Field -->
<div class="form-group col-12 text-right">
  <button type="submit" class="btn btn-<?php echo e(setting('theme_color')); ?>" ><i class="fa fa-save"></i> <?php echo e(trans('lang.save')); ?> <?php echo e(trans('lang.subcategories')); ?></button>
  <a href="<?php echo route('subcategories.index'); ?>" class="btn btn-default"><i class="fa fa-undo"></i> <?php echo e(trans('lang.cancel')); ?></a>
</div>
<?php /**PATH C:\xampp\htdocs\inesmarket_laravel\resources\views/subcategories/fields.blade.php ENDPATH**/ ?>