-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : Dim 28 fév. 2021 à 22:41
-- Version du serveur :  10.4.16-MariaDB
-- Version de PHP : 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `inesmarket_laravel`
--

-- --------------------------------------------------------

--
-- Structure de la table `app_settings`
--

CREATE TABLE `app_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `app_settings`
--

INSERT INTO `app_settings` (`id`, `key`, `value`) VALUES
(7, 'date_format', 'l jS F Y (H:i:s)'),
(8, 'language', 'fr'),
(17, 'is_human_date_format', '1'),
(18, 'app_name', 'INES MARKET'),
(19, 'app_short_description', 'Application mobile InesMarket'),
(20, 'mail_driver', 'smtp'),
(21, 'mail_host', 'smtp.hostinger.com'),
(22, 'mail_port', '587'),
(23, 'mail_username', 'productdelivery@smartersvision.com'),
(24, 'mail_password', 'NnvAwk&&E7'),
(25, 'mail_encryption', 'ssl'),
(26, 'mail_from_address', 'productdelivery@smartersvision.com'),
(27, 'mail_from_name', 'Smarter Vision'),
(30, 'timezone', 'Africa/Abidjan'),
(32, 'theme_contrast', 'light'),
(33, 'theme_color', 'primary'),
(34, 'app_logo', '6e5c6acd-44f8-4669-bfd5-2098eae62d94'),
(35, 'nav_color', 'navbar-dark bg-primary'),
(38, 'logo_bg_color', 'bg-white'),
(66, 'default_role', 'admin'),
(68, 'facebook_app_id', '518416208939727'),
(69, 'facebook_app_secret', '93649810f78fa9ca0d48972fee2a75cd'),
(71, 'twitter_app_id', 'twitter'),
(72, 'twitter_app_secret', 'twitter 1'),
(74, 'google_app_id', '527129559488-roolg8aq110p8r1q952fqa9tm06gbloe.apps.googleusercontent.com'),
(75, 'google_app_secret', 'FpIi8SLgc69ZWodk-xHaOrxn'),
(77, 'enable_google', '1'),
(78, 'enable_facebook', '1'),
(93, 'enable_stripe', '1'),
(94, 'stripe_key', 'pk_test_pltzOnX3zsUZMoTTTVUL4O41'),
(95, 'stripe_secret', 'sk_test_o98VZx3RKDUytaokX4My3a20'),
(101, 'custom_field_models.0', 'App\\Models\\User'),
(104, 'default_tax', '0'),
(107, 'default_currency', 'Frs'),
(108, 'fixed_header', 'fixed-top'),
(109, 'fixed_footer', '0'),
(110, 'fcm_key', 'AAAAHMZiAQA:APA91bEb71b5sN5jl-w_mmt6vLfgGY5-_CQFxMQsVEfcwO3FAh4-mk1dM6siZwwR3Ls9U0pRDpm96WN1AmrMHQ906GxljILqgU2ZB6Y1TjiLyAiIUETpu7pQFyicER8KLvM9JUiXcfWK'),
(111, 'enable_notifications', '1'),
(112, 'paypal_username', 'sb-z3gdq482047_api1.business.example.com'),
(113, 'paypal_password', 'JV2A7G4SEMLMZ565'),
(114, 'paypal_secret', 'AbMmSXVaig1ExpY3utVS3dcAjx7nAHH0utrZsUN6LYwPgo7wfMzrV5WZ'),
(115, 'enable_paypal', '1'),
(116, 'main_color', '#02b0f1'),
(117, 'main_dark_color', '#02b0f1'),
(118, 'second_color', '#043832'),
(119, 'second_dark_color', '#ccccdd'),
(120, 'accent_color', '#8c98a8'),
(121, 'accent_dark_color', '#9999aa'),
(122, 'scaffold_dark_color', '#2c2c2c'),
(123, 'scaffold_color', '#fafafa'),
(124, 'google_maps_key', 'AIzaSyCCiVYiUyjDnoO9nC36gYP0sEIU-Ladx3c'),
(125, 'mobile_language', 'fr'),
(126, 'app_version', '1.3.0'),
(127, 'enable_version', '0'),
(128, 'distance_unit', 'km'),
(129, 'currency_right', '1'),
(130, 'default_currency_id', '1'),
(131, 'default_currency_code', 'F CFA'),
(132, 'default_currency_decimal_digits', '0'),
(133, 'default_currency_rounding', '0');

-- --------------------------------------------------------

--
-- Structure de la table `carts`
--

CREATE TABLE `carts` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `quantity` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `carts`
--

INSERT INTO `carts` (`id`, `product_id`, `user_id`, `quantity`, `created_at`, `updated_at`) VALUES
(3, 28, 9, 1, '2020-08-14 07:38:55', '2020-08-14 10:44:00'),
(4, 28, 9, 1, '2020-08-14 07:38:56', '2020-08-14 10:44:04'),
(5, 4, 9, 15, '2020-08-14 10:43:52', '2020-08-14 10:44:15'),
(6, 28, 10, 1, '2020-08-14 17:33:26', '2020-08-14 17:35:32'),
(7, 18, 12, 1, '2020-08-26 13:51:49', '2020-08-26 13:51:49');

-- --------------------------------------------------------

--
-- Structure de la table `cart_options`
--

CREATE TABLE `cart_options` (
  `option_id` int(10) UNSIGNED NOT NULL,
  `cart_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `categories`
--

INSERT INTO `categories` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Vin', '<p>Vin<br></p>', '2021-02-28 19:25:57', '2021-02-28 19:25:57');

-- --------------------------------------------------------

--
-- Structure de la table `cdg_commune`
--

CREATE TABLE `cdg_commune` (
  `id` int(11) NOT NULL,
  `commune_name` varchar(150) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `cdg_commune`
--

INSERT INTO `cdg_commune` (`id`, `commune_name`, `updated_at`, `created_at`) VALUES
(1, 'Cocody', '2020-10-19 12:00:15', '2020-08-25 11:08:06'),
(2, 'Abobo', '2020-10-19 11:59:57', '2020-08-25 11:09:16'),
(3, 'Yopougon', '2021-02-01 13:15:14', '2020-10-19 11:45:18');

-- --------------------------------------------------------

--
-- Structure de la table `cdg_subcategories`
--

CREATE TABLE `cdg_subcategories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `IDcategorie` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `cdg_subcategories`
--

INSERT INTO `cdg_subcategories` (`id`, `name`, `description`, `IDcategorie`, `created_at`, `updated_at`) VALUES
(1, 'Bordeau', '<p>Bordeau<br></p>', 1, '2021-02-28 19:26:59', '2021-02-28 19:26:59'),
(2, 'Merlot', '<p>Merlot<br></p>', 1, '2021-02-28 19:28:00', '2021-02-28 19:28:00');

-- --------------------------------------------------------

--
-- Structure de la table `cdg_zone`
--

CREATE TABLE `cdg_zone` (
  `id` int(11) NOT NULL,
  `zone_name` varchar(150) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `cdg_zone`
--

INSERT INTO `cdg_zone` (`id`, `zone_name`, `updated_at`, `created_at`) VALUES
(1, 'Zone 1', '2020-10-09 11:14:58', '2020-10-09 11:14:58'),
(9, 'Zone 2', '2020-10-15 16:47:41', '2020-10-15 16:47:05');

-- --------------------------------------------------------

--
-- Structure de la table `currencies`
--

CREATE TABLE `currencies` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `symbol` varchar(63) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `decimal_digits` tinyint(3) UNSIGNED DEFAULT NULL,
  `rounding` tinyint(3) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `currencies`
--

INSERT INTO `currencies` (`id`, `name`, `symbol`, `code`, `decimal_digits`, `rounding`, `created_at`, `updated_at`) VALUES
(1, 'Francs', 'Frs', 'F CFA', 0, 0, '2019-10-22 15:50:48', '2020-10-03 04:35:24');

-- --------------------------------------------------------

--
-- Structure de la table `custom_fields`
--

CREATE TABLE `custom_fields` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(56) COLLATE utf8mb4_unicode_ci NOT NULL,
  `values` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disabled` tinyint(1) DEFAULT NULL,
  `required` tinyint(1) DEFAULT NULL,
  `in_table` tinyint(1) DEFAULT NULL,
  `bootstrap_column` tinyint(4) DEFAULT NULL,
  `order` tinyint(4) DEFAULT NULL,
  `custom_field_model` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `custom_fields`
--

INSERT INTO `custom_fields` (`id`, `name`, `type`, `values`, `disabled`, `required`, `in_table`, `bootstrap_column`, `order`, `custom_field_model`, `created_at`, `updated_at`) VALUES
(4, 'phone', 'text', NULL, 0, 0, 0, 6, 2, 'App\\Models\\User', '2019-09-06 21:30:00', '2019-09-06 21:31:47'),
(5, 'bio', 'textarea', NULL, 0, 0, 0, 6, 1, 'App\\Models\\User', '2019-09-06 21:43:58', '2019-09-06 21:43:58'),
(6, 'address', 'text', NULL, 0, 0, 0, 6, 3, 'App\\Models\\User', '2019-09-06 21:49:22', '2019-09-06 21:49:22');

-- --------------------------------------------------------

--
-- Structure de la table `custom_field_values`
--

CREATE TABLE `custom_field_values` (
  `id` int(10) UNSIGNED NOT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `view` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom_field_id` int(10) UNSIGNED NOT NULL,
  `customizable_type` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customizable_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `custom_field_values`
--

INSERT INTO `custom_field_values` (`id`, `value`, `view`, `custom_field_id`, `customizable_type`, `customizable_id`, `created_at`, `updated_at`) VALUES
(29, '+136 226 5669', '+136 226 5669', 4, 'App\\Models\\User', 2, '2019-09-06 21:52:30', '2019-09-06 21:52:30'),
(30, 'Lobortis mattis aliquam faucibus purus. Habitasse platea dictumst vestibulum rhoncus est pellentesque elit. Nunc vel risus commodo viverra maecenas accumsan lacus vel.', 'Lobortis mattis aliquam faucibus purus. Habitasse platea dictumst vestibulum rhoncus est pellentesque elit. Nunc vel risus commodo viverra maecenas accumsan lacus vel.', 5, 'App\\Models\\User', 2, '2019-09-06 21:52:30', '2019-10-16 19:32:35'),
(31, '2911 Corpening Drive South Lyon, MI 48178', '2911 Corpening Drive South Lyon, MI 48178', 6, 'App\\Models\\User', 2, '2019-09-06 21:52:30', '2019-10-16 19:32:35'),
(32, '+136 226 5660', '+136 226 5660', 4, 'App\\Models\\User', 1, '2019-09-06 21:53:58', '2019-09-27 08:12:04'),
(33, 'Faucibus ornare suspendisse sed nisi lacus sed. Pellentesque sit amet porttitor eget dolor morbi non arcu. Eu scelerisque felis imperdiet proin fermentum leo vel orci porta', 'Faucibus ornare suspendisse sed nisi lacus sed. Pellentesque sit amet porttitor eget dolor morbi non arcu. Eu scelerisque felis imperdiet proin fermentum leo vel orci porta', 5, 'App\\Models\\User', 1, '2019-09-06 21:53:58', '2019-10-16 19:23:53'),
(34, '569 Braxton Street Cortland, IL 60112', '569 Braxton Street Cortland, IL 60112', 6, 'App\\Models\\User', 1, '2019-09-06 21:53:58', '2019-10-16 19:23:53'),
(35, '+1 098-6543-236', '+1 098-6543-236', 4, 'App\\Models\\User', 3, '2019-10-15 17:21:32', '2019-10-17 23:21:43'),
(36, 'Aliquet porttitor lacus luctus accumsan tortor posuere ac ut. Tortor pretium viverra suspendisse', 'Aliquet porttitor lacus luctus accumsan tortor posuere ac ut. Tortor pretium viverra suspendisse', 5, 'App\\Models\\User', 3, '2019-10-15 17:21:32', '2019-10-17 23:21:12'),
(37, '1850 Big Elm Kansas City, MO 64106', '1850 Big Elm Kansas City, MO 64106', 6, 'App\\Models\\User', 3, '2019-10-15 17:21:32', '2019-10-17 23:21:43'),
(38, '+1 248-437-7610', '+1 248-437-7610', 4, 'App\\Models\\User', 4, '2019-10-16 19:31:46', '2019-10-16 19:31:46'),
(39, 'Faucibus ornare suspendisse sed nisi lacus sed. Pellentesque sit amet porttitor eget dolor morbi non arcu. Eu scelerisque felis imperdiet proin fermentum leo vel orci porta', 'Faucibus ornare suspendisse sed nisi lacus sed. Pellentesque sit amet porttitor eget dolor morbi non arcu. Eu scelerisque felis imperdiet proin fermentum leo vel orci porta', 5, 'App\\Models\\User', 4, '2019-10-16 19:31:46', '2019-10-16 19:31:46'),
(40, '1050 Frosty Lane Sidney, NY 13838', '1050 Frosty Lane Sidney, NY 13838', 6, 'App\\Models\\User', 4, '2019-10-16 19:31:46', '2019-10-16 19:31:46'),
(41, '+136 226 5669', '+136 226 5669', 4, 'App\\Models\\User', 5, '2019-12-15 18:49:44', '2019-12-15 18:49:44'),
(42, '<p>Short Bio</p>', 'Short Bio', 5, 'App\\Models\\User', 5, '2019-12-15 18:49:44', '2019-12-15 18:49:44'),
(43, '4722 Villa Drive', '4722 Villa Drive', 6, 'App\\Models\\User', 5, '2019-12-15 18:49:44', '2019-12-15 18:49:44'),
(44, '+136 955 6525', '+136 955 6525', 4, 'App\\Models\\User', 6, '2020-03-29 17:28:04', '2020-03-29 17:28:04'),
(45, '<p>Short bio for this driver</p>', 'Short bio for this driver', 5, 'App\\Models\\User', 6, '2020-03-29 17:28:05', '2020-03-29 17:28:05'),
(46, '4722 Villa Drive', '4722 Villa Drive', 6, 'App\\Models\\User', 6, '2020-03-29 17:28:05', '2020-03-29 17:28:05'),
(47, NULL, NULL, 4, 'App\\Models\\User', 7, '2020-08-13 19:27:21', '2020-08-13 19:27:21'),
(48, NULL, '', 5, 'App\\Models\\User', 7, '2020-08-13 19:27:21', '2020-08-13 19:27:21'),
(49, NULL, NULL, 6, 'App\\Models\\User', 7, '2020-08-13 19:27:21', '2020-08-13 19:27:21'),
(50, '+2209787751', '+2209787751', 4, 'App\\Models\\User', 8, '2020-08-13 23:42:17', '2020-08-13 23:42:17'),
(51, 'mlkjhgf', 'mlkjhgf', 5, 'App\\Models\\User', 8, '2020-08-13 23:42:17', '2020-08-13 23:42:17'),
(52, 'Chelsea fghtgghyh', 'Chelsea fghtgghyh', 6, 'App\\Models\\User', 8, '2020-08-13 23:42:17', '2020-08-13 23:42:17'),
(53, '+22577076538', '+22577076538', 4, 'App\\Models\\User', 10, '2020-08-14 17:34:23', '2020-08-14 17:34:23'),
(54, 'Blockhaus', 'Blockhaus', 5, 'App\\Models\\User', 10, '2020-08-14 17:34:23', '2020-08-14 17:34:55'),
(55, 'Abidjan', 'Abidjan', 6, 'App\\Models\\User', 10, '2020-08-14 17:34:23', '2020-08-14 17:34:23');

-- --------------------------------------------------------

--
-- Structure de la table `delivery_addresses`
--

CREATE TABLE `delivery_addresses` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` varchar(24) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(24) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_default` tinyint(1) DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `delivery_addresses`
--

INSERT INTO `delivery_addresses` (`id`, `description`, `address`, `latitude`, `longitude`, `is_default`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'Qui molestiae culpa nemo sit tempora.', '3814 O\'Reilly Rapid\nPort Bellehaven, FL 05116-6713', '-24.630998', '-47.953468', 1, 6, '2020-08-12 23:28:47', '2020-08-12 23:28:47'),
(2, 'Ipsum iste enim neque cumque.', '45789 Becker Row\nHaagshire, WI 10377-7574', '-52.501358', '62.818891', 1, 3, '2020-08-12 23:28:47', '2020-08-12 23:28:47'),
(3, 'Nulla cumque atque saepe iste porro qui illo non.', '80130 Heaney Land\nDenesikhaven, DE 83190', '-30.883764', '139.242176', 0, 6, '2020-08-12 23:28:47', '2020-08-12 23:28:47'),
(4, 'Voluptatum nemo iusto provident similique velit dolore.', '5568 Labadie Knolls\nLake Caraport, AZ 65505', '67.085538', '-105.278241', 0, 1, '2020-08-12 23:28:47', '2020-08-12 23:28:47'),
(5, 'Amet eum ea placeat aspernatur vero dolor.', '38420 Raphaelle Springs Suite 452\nSwaniawskistad, MO 44418-0530', '21.28872', '-135.343887', 1, 6, '2020-08-12 23:28:47', '2020-08-12 23:28:47'),
(6, 'Assumenda labore labore eveniet quae quo.', '3265 Valerie Shore Apt. 917\nNorth Javonte, ME 01450', '-45.146957', '81.590237', 0, 3, '2020-08-12 23:28:47', '2020-08-12 23:28:47'),
(7, 'Sed enim officiis nam voluptatem et.', '736 Reilly Heights Suite 768\nNorth Alishabury, NV 90587', '-39.163661', '148.68195', 0, 4, '2020-08-12 23:28:48', '2020-08-12 23:28:48'),
(8, 'Inventore sint nam libero ab a quaerat ut.', '820 Ethelyn Circles Apt. 416\nClaudineburgh, TX 64043', '-79.335633', '74.030207', 0, 5, '2020-08-12 23:28:48', '2020-08-12 23:28:48'),
(9, 'Suscipit voluptas et et ut deleniti iste.', '6211 Katlynn Village\nEast Rodolfo, MN 34381-7822', '-15.86873', '30.267507', 1, 5, '2020-08-12 23:28:48', '2020-08-12 23:28:48'),
(10, 'Deleniti quia ea aspernatur quia.', '2675 Edmond Valley Suite 158\nSouth Derrick, KY 64020', '-30.505997', '30.092561', 0, 2, '2020-08-12 23:28:48', '2020-08-12 23:28:48'),
(11, 'In adipisci sequi qui praesentium illum sed.', '7871 Fisher Highway\nGutkowskiburgh, AK 88284-2451', '27.174157', '158.496668', 0, 6, '2020-08-12 23:28:48', '2020-08-12 23:28:48'),
(12, 'Autem qui repellat nulla eius.', '7402 Earl Pass\nWest Pricemouth, IL 05015-6038', '34.636269', '-114.88051', 1, 2, '2020-08-12 23:28:48', '2020-08-12 23:28:48'),
(13, 'Incidunt est odio totam reprehenderit sint ducimus eaque.', '179 Bartell Ridge Apt. 236\nEast Malvinastad, MO 52186-1847', '-40.27706', '99.157504', 1, 5, '2020-08-12 23:28:48', '2020-08-12 23:28:48'),
(14, 'Facilis maxime ea eum.', '654 Wilford Fields\nLindgrenstad, ND 73869', '-22.557603', '46.01114', 0, 1, '2020-08-12 23:28:48', '2020-08-12 23:28:48'),
(15, 'Velit dolorem commodi rerum numquam incidunt in ducimus molestiae.', '42372 Ondricka Mountain Suite 729\nPort Brandynport, MT 50718-8878', '82.605314', '46.656337', 0, 6, '2020-08-12 23:28:48', '2020-08-12 23:28:48');

-- --------------------------------------------------------

--
-- Structure de la table `drivers`
--

CREATE TABLE `drivers` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `delivery_fee` double(5,2) NOT NULL DEFAULT 0.00,
  `total_orders` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `earning` double(9,2) NOT NULL DEFAULT 0.00,
  `available` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `drivers_payouts`
--

CREATE TABLE `drivers_payouts` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `method` varchar(127) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` double(9,2) NOT NULL DEFAULT 0.00,
  `paid_date` datetime DEFAULT NULL,
  `note` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `driver_markets`
--

CREATE TABLE `driver_markets` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `market_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `driver_markets`
--

INSERT INTO `driver_markets` (`user_id`, `market_id`) VALUES
(5, 1),
(5, 2),
(5, 4),
(6, 2),
(6, 3),
(6, 4);

-- --------------------------------------------------------

--
-- Structure de la table `earnings`
--

CREATE TABLE `earnings` (
  `id` int(10) UNSIGNED NOT NULL,
  `market_id` int(10) UNSIGNED NOT NULL,
  `total_orders` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `total_earning` double(9,2) NOT NULL DEFAULT 0.00,
  `admin_earning` double(9,2) NOT NULL DEFAULT 0.00,
  `market_earning` double(9,2) NOT NULL DEFAULT 0.00,
  `delivery_fee` double(9,2) NOT NULL DEFAULT 0.00,
  `tax` double(9,2) NOT NULL DEFAULT 0.00,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `earnings`
--

INSERT INTO `earnings` (`id`, `market_id`, `total_orders`, `total_earning`, `admin_earning`, `market_earning`, `delivery_fee`, `tax`, `created_at`, `updated_at`) VALUES
(1, 2, 0, 0.00, 0.00, 0.00, 0.00, 0.00, '2020-08-13 23:01:09', '2020-08-13 23:01:09'),
(2, 4, 0, 0.00, 0.00, 0.00, 0.00, 0.00, '2020-08-13 23:02:25', '2020-08-13 23:02:25'),
(3, 5, 0, 0.00, 0.00, 0.00, 0.00, 0.00, '2020-08-13 23:09:58', '2020-08-13 23:09:58'),
(4, 1, 0, 0.00, 0.00, 0.00, 0.00, 0.00, '2020-08-13 23:10:56', '2020-08-13 23:10:56'),
(5, 3, 0, 0.00, 0.00, 0.00, 0.00, 0.00, '2020-08-13 23:11:30', '2020-08-13 23:11:30'),
(6, 11, 0, 0.00, 0.00, 0.00, 0.00, 0.00, '2020-10-19 14:12:46', '2020-10-19 14:12:46');

-- --------------------------------------------------------

--
-- Structure de la table `faqs`
--

CREATE TABLE `faqs` (
  `id` int(10) UNSIGNED NOT NULL,
  `question` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `answer` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `faq_category_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `faqs`
--

INSERT INTO `faqs` (`id`, `question`, `answer`, `faq_category_id`, `created_at`, `updated_at`) VALUES
(1, 'Laborum est molestias dolor. Aspernatur quas aspernatur cupiditate tenetur aut tempore facilis.', 'Mouse, sharply and very soon had to double themselves up and picking the daisies, when suddenly a White Rabbit hurried by--the frightened Mouse splashed his way through the wood. \'It\'s the Cheshire.', 4, '2020-08-12 23:28:54', '2020-08-12 23:28:54'),
(4, 'Sed repellat quod quia laborum non facilis aut. Et ut ex architecto voluptatem omnis.', 'King said gravely, \'and go on till you come to an end! \'I wonder how many hours a day is very confusing.\' \'It isn\'t,\' said the Pigeon; \'but if you\'ve seen them at last, more calmly, though still.', 2, '2020-08-12 23:28:54', '2020-08-12 23:28:54'),
(6, 'Quaerat quo libero ex est dolores. Delectus quos occaecati assumenda non fugit.', 'I am very tired of this. I vote the young lady tells us a story!\' said the Gryphon. \'I\'ve forgotten the little door: but, alas! either the locks were too large, or the key was lying under the hedge.', 2, '2020-08-12 23:28:54', '2020-08-12 23:28:54'),
(10, 'Et nihil maxime sit est aut illo aut sed. Aut voluptatem nobis sed. Laboriosam non quos ab.', 'Beautiful, beautiful Soup! \'Beautiful Soup! Who cares for fish, Game, or any other dish? Who would not stoop? Soup of the court. (As that is rather a hard word, I will prosecute YOU.--Come, I\'ll.', 4, '2020-08-12 23:28:55', '2020-08-12 23:28:55'),
(11, 'Doloremque dolore quaerat in voluptatem et. Vel fuga qui qui qui recusandae quia repudiandae at.', 'Alice crouched down among the distant green leaves. As there seemed to have been that,\' said the Duchess, who seemed to be sure; but I think it would be so easily offended!\' \'You\'ll get used to.', 2, '2020-08-12 23:28:55', '2020-08-12 23:28:55'),
(15, 'Hic deleniti et laudantium itaque vel suscipit. Ratione et quibusdam ipsam nesciunt.', 'All on a branch of a candle is blown out, for she felt that she began again: \'Ou est ma chatte?\' which was sitting on the ground as she listened, or seemed to be a walrus or hippopotamus, but then.', 2, '2020-08-12 23:28:56', '2020-08-12 23:28:56'),
(17, 'Quo enim ut iure in molestias debitis. Et molestiae ea incidunt. Aut ut reprehenderit et.', 'After a while she remembered how small she was appealed to by the way out of the soldiers remaining behind to execute the unfortunate gardeners, who ran to Alice to herself, \'to be going messages.', 4, '2020-08-12 23:28:56', '2020-08-12 23:28:56'),
(18, 'Molestiae recusandae vel unde suscipit est. Est deserunt est molestias quo nostrum quia voluptas.', 'Mock Turtle a little timidly, \'why you are very dull!\' \'You ought to tell me who YOU are, first.\' \'Why?\' said the Mouse, frowning, but very glad that it had been. But her sister on the bank--the.', 2, '2020-08-12 23:28:56', '2020-08-12 23:28:56'),
(19, 'Excepturi autem velit laboriosam eos provident saepe sequi. Accusamus esse ducimus nobis qui porro.', 'There could be no chance of her favourite word \'moral,\' and the Hatter continued, \'in this way:-- \"Up above the world go round!\"\' \'Somebody said,\' Alice whispered, \'that it\'s done by everybody.', 4, '2020-08-12 23:28:56', '2020-08-12 23:28:56'),
(22, 'Iste doloribus tempore aliquid tempora. Sunt at velit ab perferendis culpa quam tempora.', 'Mystery,\' the Mock Turtle sang this, very slowly and sadly:-- \'\"Will you walk a little feeble, squeaking voice, (\'That\'s Bill,\' thought Alice,) \'Well, I shan\'t grow any more--As it is, I can\'t show.', 4, '2020-08-12 23:28:56', '2020-08-12 23:28:56'),
(25, 'Et est fuga id. Magnam maiores qui aperiam quasi. Ab veniam exercitationem corrupti enim odit.', 'Knave \'Turn them over!\' The Knave of Hearts, carrying the King\'s crown on a summer day: The Knave of Hearts, she made it out to sea!\" But the snail replied \"Too far, too far!\" and gave a sudden.', 4, '2020-08-12 23:28:56', '2020-08-12 23:28:56'),
(27, 'Enim est nemo quas enim ab quia consequatur tempora. Accusamus sint corporis optio maiores vel.', 'I am now? That\'ll be a very humble tone, going down on one knee as he shook his head contemptuously. \'I dare say there may be ONE.\' \'One, indeed!\' said the youth, \'one would hardly suppose That your.', 4, '2020-08-12 23:28:56', '2020-08-12 23:28:56'),
(29, 'Voluptates exercitationem harum sit labore rerum ab. Accusamus doloremque ab eum quia quia.', 'Gryphon. \'Do you mean that you think you\'re changed, do you?\' \'I\'m afraid I\'ve offended it again!\' For the Mouse with an M--\' \'Why with an M, such as mouse-traps, and the words \'DRINK ME\'.', 2, '2020-08-12 23:28:56', '2020-08-12 23:28:56'),
(30, 'Quia et commodi est id. Nisi nemo enim sapiente dolores assumenda quos omnis assumenda.', 'Alice\'s, and they can\'t prove I did: there\'s no name signed at the Duchess said to herself, in a great many teeth, so she tried to say when I got up and down in a very good height indeed!\' said the.', 4, '2020-08-12 23:28:56', '2020-08-12 23:28:56');

-- --------------------------------------------------------

--
-- Structure de la table `faq_categories`
--

CREATE TABLE `faq_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `faq_categories`
--

INSERT INTO `faq_categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
(2, 'Services', '2019-08-31 12:32:03', '2019-08-31 12:32:03'),
(4, 'Misc', '2019-08-31 12:32:17', '2019-08-31 12:32:17');

-- --------------------------------------------------------

--
-- Structure de la table `favorites`
--

CREATE TABLE `favorites` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `favorites`
--

INSERT INTO `favorites` (`id`, `product_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 21, 3, '2020-08-12 23:28:57', '2020-08-12 23:28:57'),
(3, 28, 3, '2020-08-12 23:28:57', '2020-08-12 23:28:57'),
(5, 4, 1, '2020-08-12 23:28:57', '2020-08-12 23:28:57'),
(9, 1, 6, '2020-08-12 23:28:57', '2020-08-12 23:28:57'),
(11, 28, 4, '2020-08-12 23:28:57', '2020-08-12 23:28:57'),
(22, 1, 6, '2020-08-12 23:28:58', '2020-08-12 23:28:58'),
(31, 28, 8, '2020-08-13 23:37:34', '2020-08-13 23:37:34'),
(32, 27, 9, '2020-08-14 12:45:38', '2020-08-14 12:45:38');

-- --------------------------------------------------------

--
-- Structure de la table `favorite_options`
--

CREATE TABLE `favorite_options` (
  `option_id` int(10) UNSIGNED NOT NULL,
  `favorite_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `fields`
--

CREATE TABLE `fields` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `fields`
--

INSERT INTO `fields` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Grocery', 'Eum similique maiores atque quia explicabo. Dolores quia placeat consequatur id quis perspiciatis. Ducimus sit ducimus officia labore maiores et porro. Est iusto natus nesciunt debitis consequuntur totam. Et illo et autem inventore earum corrupti.', '2020-04-11 15:03:21', '2020-04-11 15:03:21'),
(2, 'Pharmacy', 'Eaque et aut natus. Minima blanditiis ut sunt distinctio ad. Quasi doloremque rerum ex rerum. Molestias similique similique aut rerum delectus blanditiis et. Dolorem et quas nostrum est nobis.', '2020-04-11 15:03:21', '2020-04-11 15:03:21'),
(3, 'Restaurant', 'Est nihil omnis natus ducimus ducimus excepturi quos. Et praesentium in quia veniam. Tempore aut nesciunt consequatur pariatur recusandae. Voluptatem commodi eius quaerat est deleniti impedit. Qui quo harum est sequi incidunt labore eligendi cum.', '2020-04-11 15:03:21', '2020-04-11 15:03:21'),
(4, 'Store', 'Ex nostrum suscipit aut et labore. Ut dolor ut eum eum voluptatem ex. Sapiente in tempora soluta voluptatem. Officia accusantium quae sit. Rerum esse ipsa molestias dolorem et est autem consequatur.', '2020-04-11 15:03:21', '2020-04-11 15:03:21'),
(5, 'Electronics', 'Dolorum earum ut blanditiis blanditiis. Facere quis voluptates assumenda saepe. Ab aspernatur voluptatibus rem doloremque cum impedit. Itaque blanditiis commodi repudiandae asperiores. Modi atque placeat consectetur et aut blanditiis.', '2020-04-11 15:03:21', '2020-04-11 15:03:21'),
(6, 'Furniture', 'Est et iste enim. Quam repudiandae commodi rerum non esse. Et in aut sequi est aspernatur. Facere non modi expedita asperiores. Ipsa laborum saepe deserunt qui consequatur voluptas inventore dolorum.', '2020-04-11 15:03:21', '2020-04-11 15:03:21');

-- --------------------------------------------------------

--
-- Structure de la table `galleries`
--

CREATE TABLE `galleries` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `market_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `galleries`
--

INSERT INTO `galleries` (`id`, `description`, `market_id`, `created_at`, `updated_at`) VALUES
(2, 'Id cumque exercitationem ut fugit nobis ipsam ducimus.', 2, '2020-08-12 23:28:40', '2020-08-12 23:28:40'),
(3, 'Eveniet velit dignissimos sint aut.', 1, '2020-08-12 23:28:41', '2020-08-12 23:28:41'),
(5, 'Et et hic quibusdam molestiae consequatur.', 4, '2020-08-12 23:28:41', '2020-08-12 23:28:41'),
(6, 'Totam nam voluptatem a commodi sed quidem.', 4, '2020-08-12 23:28:41', '2020-08-12 23:28:41'),
(8, 'Laboriosam doloribus rem adipisci beatae aperiam.', 5, '2020-08-12 23:28:41', '2020-08-12 23:28:41'),
(12, 'Iusto quis voluptatem qui optio ea voluptas explicabo reprehenderit.', 3, '2020-08-12 23:28:41', '2020-08-12 23:28:41'),
(13, 'Veritatis eos ipsum sunt.', 5, '2020-08-12 23:28:41', '2020-08-12 23:28:41'),
(14, 'Nihil voluptatem reiciendis optio aspernatur laboriosam et et.', 3, '2020-08-12 23:28:41', '2020-08-12 23:28:41');

-- --------------------------------------------------------

--
-- Structure de la table `markets`
--

CREATE TABLE `markets` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` varchar(24) COLLATE utf8mb4_unicode_ci NOT NULL,
  `longitude` varchar(24) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `information` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `admin_commission` double(8,2) DEFAULT 0.00,
  `delivery_fee` double(8,2) DEFAULT 0.00,
  `delivery_range` double(8,2) DEFAULT 0.00,
  `default_tax` double(8,2) DEFAULT 0.00,
  `closed` tinyint(1) DEFAULT 0,
  `available_for_delivery` tinyint(1) DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `communeID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `markets`
--

INSERT INTO `markets` (`id`, `name`, `description`, `address`, `latitude`, `longitude`, `phone`, `mobile`, `information`, `admin_commission`, `delivery_fee`, `delivery_range`, `default_tax`, `closed`, `available_for_delivery`, `created_at`, `updated_at`, `communeID`) VALUES
(1, 'Cash Ivoir', 'Molestiae aut perspiciatis nam deserunt omnis et eos. Rerum praesentium quis magnam non. Est dolor qui ex. Laboriosam sequi molestiae saepe voluptate quo excepturi.', '96166 Fanny Ports Apt. 374Rohanhaven, NV 27683-1930', '48.997541', '10.317703', '+1-663-910-2935', '934.204.0677 x247', 'Ut aperiam dolores dignissimos autem. Libero sed quia cum reprehenderit. Nesciunt illum illum necessitatibus odio delectus et quos.', 38.51, 9.77, 53.82, 5.15, 1, 0, '2020-08-12 23:28:34', '2020-08-13 23:10:56', 1),
(2, 'King Cash', 'Eum eveniet quaerat magnam omnis ea odit. Voluptatem blanditiis qui asperiores voluptatem. Ea error necessitatibus quae ut. Ut molestiae architecto similique laborum aliquid et.', '20480 Grant Corners Suite 733New Adolf, TN 38771', '50.206249', '9.261066', '+1-291-206-8207', '1-687-227-6502 x607', 'Exercitationem quasi quibusdam exercitationem at error libero. Incidunt sunt nemo rem quasi quae. Corporis sint est aliquid ipsam.', 12.98, 8.67, 15.41, 8.04, 0, 1, '2020-08-12 23:28:35', '2020-08-13 23:01:09', 1),
(3, 'Bon Prix', 'Id qui qui autem eos quis consequatur blanditiis. Aut totam fuga repellendus et rerum. Praesentium labore omnis quia incidunt vel necessitatibus. Modi fuga velit ipsam recusandae.', '5296 Lonie RoadAuroreland, KS 68814', '37.252711', '10.611085', '+12654624784', '1-683-362-1119', 'Suscipit voluptate sapiente dolorum quis. Et sed doloremque sed. Ratione veritatis libero et distinctio at beatae.', 29.61, 8.48, 81.82, 19.91, 1, 1, '2020-08-12 23:28:35', '2020-08-13 23:11:30', 2),
(4, 'Carrefour', 'Consequatur magnam provident quia laborum. Suscipit dolores itaque nemo est eos. Eligendi qui est rerum rerum. Deserunt aut dicta ex harum.', '6947 Gleason BrookKenyattafort, DE 42606-5330', '49.643465', '10.668473', '425.814.3028 x879', '+1-746-374-0955', 'Odit minima non minima in. Atque dolores corrupti dolorem. Officia temporibus adipisci omnis esse.', 34.19, 6.28, 78.97, 8.10, 0, 0, '2020-08-12 23:28:35', '2020-08-13 23:02:25', 3),
(5, 'Prosuma', 'Laudantium facere nihil veniam quaerat labore. Sunt similique earum officiis est enim at. Quia cumque optio iusto possimus.', '1411 Lubowitz Forge Suite 066East Corrinefurt, KS 27245', '38.77554', '11.483007', '465.424.9486', '(712) 754-5122', 'Tempore eum veniam qui labore dolor in nobis. Est ratione dolores explicabo. Iure vel atque ut atque voluptates ab quod.', 48.75, 6.17, 79.83, 29.14, 0, 1, '2020-08-12 23:28:35', '2020-08-13 23:09:58', 2),
(11, 'Lorem Ipsum', '<p>Lorem Ipsum test<br></p>', 'Abidjan Plateau Dokui', '0000000000', '000000000000000000000000', NULL, NULL, NULL, 30.00, NULL, NULL, NULL, 0, 1, '2020-10-19 14:12:45', '2020-10-19 14:29:33', 3);

-- --------------------------------------------------------

--
-- Structure de la table `markets_payouts`
--

CREATE TABLE `markets_payouts` (
  `id` int(10) UNSIGNED NOT NULL,
  `market_id` int(10) UNSIGNED NOT NULL,
  `method` varchar(127) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` double(9,2) NOT NULL DEFAULT 0.00,
  `paid_date` datetime DEFAULT NULL,
  `note` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `market_fields`
--

CREATE TABLE `market_fields` (
  `field_id` int(10) UNSIGNED NOT NULL,
  `market_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `market_fields`
--

INSERT INTO `market_fields` (`field_id`, `market_id`) VALUES
(2, 1),
(2, 2),
(2, 11),
(3, 2),
(4, 1),
(4, 3),
(6, 5);

-- --------------------------------------------------------

--
-- Structure de la table `market_reviews`
--

CREATE TABLE `market_reviews` (
  `id` int(10) UNSIGNED NOT NULL,
  `review` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rate` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `user_id` int(10) UNSIGNED NOT NULL,
  `market_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `media`
--

CREATE TABLE `media` (
  `id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL,
  `collection_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mime_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disk` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` int(10) UNSIGNED NOT NULL,
  `manipulations` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `custom_properties` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `responsive_images` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_column` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `media`
--

INSERT INTO `media` (`id`, `model_type`, `model_id`, `collection_name`, `name`, `file_name`, `mime_type`, `disk`, `size`, `manipulations`, `custom_properties`, `responsive_images`, `order_column`, `created_at`, `updated_at`) VALUES
(1, 'App\\Models\\Upload', 1, 'app_logo', '512', '512.png', 'image/png', 'public', 32150, '[]', '{\"uuid\":\"6e5c6acd-44f8-4669-bfd5-2098eae62d94\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 1, '2020-08-13 19:19:55', '2020-08-13 19:20:01'),
(2, 'App\\Models\\Upload', 2, 'image', '5cb0515476f95-6logo-king-cash-siprom-cote-ivoire', '5cb0515476f95-6logo-king-cash-siprom-cote-ivoire.png', 'image/png', 'public', 6371, '[]', '{\"uuid\":\"e095af18-01fd-46a0-a153-090847a7900e\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 2, '2020-08-13 23:07:15', '2020-08-13 23:07:20'),
(3, 'App\\Models\\Market', 2, 'image', '5cb0515476f95-6logo-king-cash-siprom-cote-ivoire', '5cb0515476f95-6logo-king-cash-siprom-cote-ivoire.png', 'image/png', 'public', 6371, '[]', '{\"uuid\":\"e095af18-01fd-46a0-a153-090847a7900e\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 3, '2020-08-13 23:07:20', '2020-08-13 23:07:20'),
(4, 'App\\Models\\Upload', 3, 'image', '1200px-Carrefour_logo.svg', '1200px-Carrefour_logo.svg.png', 'image/png', 'public', 67478, '[]', '{\"uuid\":\"ca1e4b11-5170-45d5-828a-1f5948ea9190\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 4, '2020-08-13 23:08:14', '2020-08-13 23:08:15'),
(5, 'App\\Models\\Market', 4, 'image', '1200px-Carrefour_logo.svg', '1200px-Carrefour_logo.svg.png', 'image/png', 'public', 67478, '[]', '{\"uuid\":\"ca1e4b11-5170-45d5-828a-1f5948ea9190\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 5, '2020-08-13 23:09:13', '2020-08-13 23:09:13'),
(6, 'App\\Models\\Upload', 4, 'image', 'images', 'images.png', 'image/png', 'public', 5182, '[]', '{\"uuid\":\"3d752b46-5d01-46cb-97f7-1b0b2b67b632\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 6, '2020-08-13 23:09:48', '2020-08-13 23:09:48'),
(7, 'App\\Models\\Market', 5, 'image', 'images', 'images.png', 'image/png', 'public', 5182, '[]', '{\"uuid\":\"3d752b46-5d01-46cb-97f7-1b0b2b67b632\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 7, '2020-08-13 23:09:58', '2020-08-13 23:09:58'),
(8, 'App\\Models\\Upload', 5, 'image', '5cb050ffd0198-3logo-cash-ivoire-siprom-cote-ivoire', '5cb050ffd0198-3logo-cash-ivoire-siprom-cote-ivoire.jpg', 'image/jpeg', 'public', 48274, '[]', '{\"uuid\":\"de745709-937e-44ad-bb95-bd3d5809e2f3\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 8, '2020-08-13 23:10:41', '2020-08-13 23:10:41'),
(9, 'App\\Models\\Market', 1, 'image', '5cb050ffd0198-3logo-cash-ivoire-siprom-cote-ivoire', '5cb050ffd0198-3logo-cash-ivoire-siprom-cote-ivoire.jpg', 'image/jpeg', 'public', 48274, '[]', '{\"uuid\":\"de745709-937e-44ad-bb95-bd3d5809e2f3\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 9, '2020-08-13 23:10:56', '2020-08-13 23:10:56'),
(10, 'App\\Models\\Upload', 6, 'image', 'images', 'images.jpg', 'image/jpeg', 'public', 4274, '[]', '{\"uuid\":\"6b5fc961-d625-4aa7-8de5-93a6450fa1bf\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 10, '2020-08-13 23:11:14', '2020-08-13 23:11:14'),
(11, 'App\\Models\\Market', 3, 'image', 'images', 'images.jpg', 'image/jpeg', 'public', 4274, '[]', '{\"uuid\":\"6b5fc961-d625-4aa7-8de5-93a6450fa1bf\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 11, '2020-08-13 23:11:30', '2020-08-13 23:11:30'),
(12, 'App\\Models\\User', 8, 'avatar', 'avatar_default_temp', 'avatar_default_temp.png', 'image/png', 'public', 2011, '[]', '{\"uuid\":\"$2y$10$7tFkpTEDCK7YJFdftzmmvuA.Kqu0zZoM4HJgLzBh3xIiA22V\\/6EqC\",\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 12, '2020-08-13 23:36:35', '2020-08-13 23:36:35'),
(13, 'App\\Models\\User', 9, 'avatar', 'avatar_default_temp', 'avatar_default_temp.png', 'image/png', 'public', 2011, '[]', '{\"uuid\":\"$2y$10$G1w5DYDZffx7SkVI.wwCnONBvnwfPiF1Qwa0knKjXlPrbLWJINPMG\",\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 13, '2020-08-14 07:36:51', '2020-08-14 07:36:56'),
(14, 'App\\Models\\User', 10, 'avatar', 'avatar_default_temp', 'avatar_default_temp.png', 'image/png', 'public', 2011, '[]', '{\"uuid\":\"$2y$10$Hh6MGw0iJ1Enapc71SJsWuP0fUkzkDUUQx1SuFtye\\/.KWwfBDiHui\",\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 14, '2020-08-14 17:33:09', '2020-08-14 17:33:13'),
(15, 'App\\Models\\Upload', 7, 'image', 'VIN', 'VIN.jpg', 'image/jpeg', 'public', 3786, '[]', '{\"uuid\":\"aa32f3be-a8f5-4b37-a230-9370cb47d9af\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 15, '2020-08-14 21:37:20', '2020-08-14 21:37:21'),
(16, 'App\\Models\\Category', 2, 'image', 'VIN', 'VIN.jpg', 'image/jpeg', 'public', 3786, '[]', '{\"uuid\":\"aa32f3be-a8f5-4b37-a230-9370cb47d9af\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 16, '2020-08-14 21:37:26', '2020-08-14 21:37:26'),
(17, 'App\\Models\\User', 11, 'avatar', 'avatar_default_temp', 'avatar_default_temp.png', 'image/png', 'public', 2011, '[]', '{\"uuid\":\"$2y$10$YxOP9iReahcNLDHyO5iWAu3QRKEIak3hbcLR1\\/11GxNfWQmgTEqsG\",\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 17, '2020-08-16 16:41:49', '2020-08-16 16:41:54'),
(18, 'App\\Models\\Upload', 8, 'image', 'bb', 'bb.jpg', 'image/jpeg', 'public', 7740, '[]', '{\"uuid\":\"16d2471d-4aba-4b4a-8131-c3d8ff0824ec\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 18, '2020-08-16 22:22:03', '2020-08-16 22:22:07'),
(19, 'App\\Models\\Category', 7, 'image', 'bb', 'bb.jpg', 'image/jpeg', 'public', 7740, '[]', '{\"uuid\":\"16d2471d-4aba-4b4a-8131-c3d8ff0824ec\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 19, '2020-08-16 22:23:56', '2020-08-16 22:23:56'),
(20, 'App\\Models\\Upload', 9, 'image', 'Couches BB', 'Couches-BB.jpg', 'image/jpeg', 'public', 4630, '[]', '{\"uuid\":\"56ea51ac-e9ad-4cae-a692-184504dbef02\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 20, '2020-08-16 22:29:25', '2020-08-16 22:29:25'),
(21, 'App\\Models\\Upload', 10, 'image', 'Couches BB', 'Couches-BB.jpg', 'image/jpeg', 'public', 4630, '[]', '{\"uuid\":\"e9996c8d-09e1-4573-bd59-e0dc1b864b01\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 21, '2020-08-16 22:30:22', '2020-08-16 22:30:22'),
(22, 'App\\Models\\Category', 8, 'image', 'Couches BB', 'Couches-BB.jpg', 'image/jpeg', 'public', 4630, '[]', '{\"uuid\":\"e9996c8d-09e1-4573-bd59-e0dc1b864b01\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 22, '2020-08-16 22:30:31', '2020-08-16 22:30:31'),
(23, 'App\\Models\\Upload', 11, 'image', 'smiley', 'smiley.jpg', 'image/jpeg', 'public', 49552, '[]', '{\"uuid\":\"dd46b237-c84b-409b-aaa9-2ab2d81edac9\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 23, '2020-08-16 22:32:09', '2020-08-16 22:32:09'),
(24, 'App\\Models\\Product', 41, 'image', 'smiley', 'smiley.jpg', 'image/jpeg', 'public', 49552, '[]', '{\"uuid\":\"dd46b237-c84b-409b-aaa9-2ab2d81edac9\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 24, '2020-08-16 22:36:22', '2020-08-16 22:36:22'),
(25, 'App\\Models\\Upload', 12, 'image', 'VIN', 'VIN.jpg', 'image/jpeg', 'public', 3786, '[]', '{\"uuid\":\"8f1467bc-c530-4b66-bb8f-15206a1773da\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 25, '2020-08-16 22:47:34', '2020-08-16 22:47:34'),
(27, 'App\\Models\\Upload', 13, 'image', 'Vin', 'Vin.jpg', 'image/jpeg', 'public', 6552, '[]', '{\"uuid\":\"25fe70e8-89cd-4248-adab-5ed3cc539245\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 26, '2020-08-16 22:49:27', '2020-08-16 22:49:28'),
(28, 'App\\Models\\Category', 9, 'image', 'Vin', 'Vin.jpg', 'image/jpeg', 'public', 6552, '[]', '{\"uuid\":\"25fe70e8-89cd-4248-adab-5ed3cc539245\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 27, '2020-08-16 22:49:30', '2020-08-16 22:49:30'),
(29, 'App\\Models\\Upload', 14, 'image', 'CORBIERE LA CAVE D\'AUGUSTIN', 'CORBIERE-LA-CAVE-D\'AUGUSTIN.jpg', 'image/jpeg', 'public', 8385, '[]', '{\"uuid\":\"522f38d0-15d3-48d5-b169-0b8f87ece256\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 28, '2020-08-16 22:52:29', '2020-08-16 22:52:30'),
(30, 'App\\Models\\Product', 42, 'image', 'CORBIERE LA CAVE D\'AUGUSTIN', 'CORBIERE-LA-CAVE-D\'AUGUSTIN.jpg', 'image/jpeg', 'public', 8385, '[]', '{\"uuid\":\"522f38d0-15d3-48d5-b169-0b8f87ece256\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 29, '2020-08-16 22:57:44', '2020-08-16 22:57:44'),
(31, 'App\\Models\\Upload', 15, 'image', 'Bordeaux 3L', 'Bordeaux-3L.jpg', 'image/jpeg', 'public', 322246, '[]', '{\"uuid\":\"e8811090-8db5-4dff-b663-12b8ea07756b\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 30, '2020-08-16 23:01:46', '2020-08-16 23:01:52'),
(32, 'App\\Models\\Upload', 16, 'image', 'Bordeaux 3L', 'Bordeaux-3L.jpg', 'image/jpeg', 'public', 322246, '[]', '{\"uuid\":\"e7c43876-e65b-4964-88c3-52d6cafcee91\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 31, '2020-08-16 23:02:34', '2020-08-16 23:02:35'),
(33, 'App\\Models\\Product', 43, 'image', 'Bordeaux 3L', 'Bordeaux-3L.jpg', 'image/jpeg', 'public', 322246, '[]', '{\"uuid\":\"e7c43876-e65b-4964-88c3-52d6cafcee91\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 32, '2020-08-16 23:02:37', '2020-08-16 23:02:37'),
(34, 'App\\Models\\Upload', 17, 'image', 'Cote de bourg', 'Cote-de-bourg.jpg', 'image/jpeg', 'public', 8814, '[]', '{\"uuid\":\"183cd04b-6048-4818-8b80-7fd21f269b3d\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 33, '2020-08-16 23:04:48', '2020-08-16 23:04:48'),
(35, 'App\\Models\\Product', 44, 'image', 'Cote de bourg', 'Cote-de-bourg.jpg', 'image/jpeg', 'public', 8814, '[]', '{\"uuid\":\"183cd04b-6048-4818-8b80-7fd21f269b3d\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 34, '2020-08-16 23:08:30', '2020-08-16 23:08:30'),
(36, 'App\\Models\\Upload', 18, 'image', 'contri-corte-viola-terre-dell-isola-syrah-sicilia-igt-sicily-italy-10427794-1-1.jpglllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllll', 'contri-corte-viola-terre-dell-isola-syrah-sicilia-igt-sicily-italy-10427794-1-1.jpglllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllll.jpg', 'image/jpeg', 'public', 13717, '[]', '{\"uuid\":\"4fd23d06-508b-4be7-bc54-26dc5e8e0b12\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 35, '2020-08-16 23:15:08', '2020-08-16 23:15:08'),
(37, 'App\\Models\\Product', 45, 'image', 'contri-corte-viola-terre-dell-isola-syrah-sicilia-igt-sicily-italy-10427794-1-1.jpglllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllll', 'contri-corte-viola-terre-dell-isola-syrah-sicilia-igt-sicily-italy-10427794-1-1.jpglllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllll.jpg', 'image/jpeg', 'public', 13717, '[]', '{\"uuid\":\"4fd23d06-508b-4be7-bc54-26dc5e8e0b12\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 36, '2020-08-16 23:17:00', '2020-08-16 23:17:00'),
(38, 'App\\Models\\Upload', 19, 'image', 'NERO D\'AVOLA', 'NERO-D\'AVOLA.jpg', 'image/jpeg', 'public', 33147, '[]', '{\"uuid\":\"f11ff95f-629a-4688-91ad-3631a8dd8432\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 37, '2020-08-16 23:27:22', '2020-08-16 23:27:22'),
(39, 'App\\Models\\Product', 46, 'image', 'NERO D\'AVOLA', 'NERO-D\'AVOLA.jpg', 'image/jpeg', 'public', 33147, '[]', '{\"uuid\":\"f11ff95f-629a-4688-91ad-3631a8dd8432\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 38, '2020-08-16 23:31:37', '2020-08-16 23:31:37'),
(40, 'App\\Models\\Upload', 20, 'image', 'Conseils-pour-composer-un-petit-dejeuner-ideal', 'Conseils-pour-composer-un-petit-dejeuner-ideal.jpg', 'image/jpeg', 'public', 55805, '[]', '{\"uuid\":\"30492009-2b64-4b98-8930-95e59c901bb1\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 39, '2020-08-16 23:38:50', '2020-08-16 23:38:50'),
(41, 'App\\Models\\Category', 11, 'image', 'Conseils-pour-composer-un-petit-dejeuner-ideal', 'Conseils-pour-composer-un-petit-dejeuner-ideal.jpg', 'image/jpeg', 'public', 55805, '[]', '{\"uuid\":\"30492009-2b64-4b98-8930-95e59c901bb1\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 40, '2020-08-16 23:38:58', '2020-08-16 23:38:58'),
(42, 'App\\Models\\Upload', 21, 'image', 'CAFE', 'CAFE.jpg', 'image/jpeg', 'public', 11128, '[]', '{\"uuid\":\"b74217c0-9136-4262-be8c-79119bc206ba\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 41, '2020-08-16 23:43:41', '2020-08-16 23:43:41'),
(43, 'App\\Models\\Category', 12, 'image', 'CAFE', 'CAFE.jpg', 'image/jpeg', 'public', 11128, '[]', '{\"uuid\":\"b74217c0-9136-4262-be8c-79119bc206ba\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 42, '2020-08-16 23:43:44', '2020-08-16 23:43:44'),
(44, 'App\\Models\\Upload', 22, 'image', 'tassimo-grand-mere-expresso', 'tassimo-grand-mere-expresso.jpg', 'image/jpeg', 'public', 52995, '[]', '{\"uuid\":\"3537a0c6-cb68-43a5-bcf1-87eb9472a353\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 43, '2020-08-16 23:45:18', '2020-08-16 23:45:18'),
(45, 'App\\Models\\Product', 47, 'image', 'tassimo-grand-mere-expresso', 'tassimo-grand-mere-expresso.jpg', 'image/jpeg', 'public', 52995, '[]', '{\"uuid\":\"3537a0c6-cb68-43a5-bcf1-87eb9472a353\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 44, '2020-08-16 23:49:38', '2020-08-16 23:49:38'),
(46, 'App\\Models\\Upload', 23, 'image', '41U37-d1iQL._AC_', '41U37-d1iQL._AC_.jpg', 'image/jpeg', 'public', 20893, '[]', '{\"uuid\":\"bce0f055-31bd-4711-9878-f92a269ffbb1\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 45, '2020-08-16 23:55:39', '2020-08-16 23:55:39'),
(47, 'App\\Models\\Product', 48, 'image', '41U37-d1iQL._AC_', '41U37-d1iQL._AC_.jpg', 'image/jpeg', 'public', 20893, '[]', '{\"uuid\":\"bce0f055-31bd-4711-9878-f92a269ffbb1\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 46, '2020-08-16 23:56:21', '2020-08-16 23:56:21'),
(48, 'App\\Models\\Upload', 24, 'image', 'OIP', 'OIP.jpg', 'image/jpeg', 'public', 9359, '[]', '{\"uuid\":\"6b761dac-feeb-4e94-9113-f518c4c3d91f\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 47, '2020-08-19 14:11:27', '2020-08-19 14:11:29'),
(49, 'App\\Models\\Category', 3, 'image', 'OIP', 'OIP.jpg', 'image/jpeg', 'public', 9359, '[]', '{\"uuid\":\"6b761dac-feeb-4e94-9113-f518c4c3d91f\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 48, '2020-08-19 14:11:39', '2020-08-19 14:11:39'),
(50, 'App\\Models\\Upload', 25, 'image', 'Maxi 170g', 'Maxi-170g.jpg', 'image/jpeg', 'public', 30047, '[]', '{\"uuid\":\"3dd8e32b-70fb-48fe-b3b7-e75ee107add3\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 49, '2020-08-19 14:14:56', '2020-08-19 14:14:56'),
(51, 'App\\Models\\Product', 49, 'image', 'Maxi 170g', 'Maxi-170g.jpg', 'image/jpeg', 'public', 30047, '[]', '{\"uuid\":\"3dd8e32b-70fb-48fe-b3b7-e75ee107add3\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 50, '2020-08-19 14:18:09', '2020-08-19 14:18:09'),
(52, 'App\\Models\\Upload', 26, 'image', 'Maxi 170g', 'Maxi-170g.jpg', 'image/jpeg', 'public', 30047, '[]', '{\"uuid\":\"6a329678-304e-46f5-90b9-b5a1d4b2cf89\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 51, '2020-08-19 14:18:33', '2020-08-19 14:18:34'),
(53, 'App\\Models\\Product', 50, 'image', 'Maxi 170g', 'Maxi-170g.jpg', 'image/jpeg', 'public', 30047, '[]', '{\"uuid\":\"6a329678-304e-46f5-90b9-b5a1d4b2cf89\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 52, '2020-08-19 14:19:31', '2020-08-19 14:19:31'),
(54, 'App\\Models\\Upload', 27, 'image', 'Maxi 170g', 'Maxi-170g.jpg', 'image/jpeg', 'public', 30047, '[]', '{\"uuid\":\"db174ff1-64ff-4812-aeb8-005afc5d04e7\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 53, '2020-08-19 14:20:11', '2020-08-19 14:20:11'),
(55, 'App\\Models\\Product', 51, 'image', 'Maxi 170g', 'Maxi-170g.jpg', 'image/jpeg', 'public', 30047, '[]', '{\"uuid\":\"db174ff1-64ff-4812-aeb8-005afc5d04e7\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 54, '2020-08-19 14:21:02', '2020-08-19 14:21:02'),
(56, 'App\\Models\\Upload', 28, 'image', 'Maxi 170g', 'Maxi-170g.jpg', 'image/jpeg', 'public', 30047, '[]', '{\"uuid\":\"d901f912-e981-4c42-bc70-2a1aa935daa0\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 55, '2020-08-19 14:21:18', '2020-08-19 14:21:19'),
(57, 'App\\Models\\Product', 52, 'image', 'Maxi 170g', 'Maxi-170g.jpg', 'image/jpeg', 'public', 30047, '[]', '{\"uuid\":\"d901f912-e981-4c42-bc70-2a1aa935daa0\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 56, '2020-08-19 14:22:10', '2020-08-19 14:22:10'),
(58, 'App\\Models\\Upload', 29, 'image', 'Ideal plus', 'Ideal-plus.jpg', 'image/jpeg', 'public', 28482, '[]', '{\"uuid\":\"093a030e-d496-4edd-81d7-966161fb560c\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 57, '2020-08-19 14:25:26', '2020-08-19 14:25:26'),
(59, 'App\\Models\\Product', 53, 'image', 'Ideal plus', 'Ideal-plus.jpg', 'image/jpeg', 'public', 28482, '[]', '{\"uuid\":\"093a030e-d496-4edd-81d7-966161fb560c\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 58, '2020-08-19 14:26:18', '2020-08-19 14:26:18'),
(60, 'App\\Models\\Upload', 30, 'image', 'Ideal plus', 'Ideal-plus.jpg', 'image/jpeg', 'public', 28482, '[]', '{\"uuid\":\"cfdf4fc3-4209-4fbb-98e5-91ec6ff0b7fa\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 59, '2020-08-19 14:27:02', '2020-08-19 14:27:02'),
(61, 'App\\Models\\Product', 54, 'image', 'Ideal plus', 'Ideal-plus.jpg', 'image/jpeg', 'public', 28482, '[]', '{\"uuid\":\"cfdf4fc3-4209-4fbb-98e5-91ec6ff0b7fa\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 60, '2020-08-19 14:27:43', '2020-08-19 14:27:43'),
(62, 'App\\Models\\User', 12, 'avatar', 'avatar_default_temp', 'avatar_default_temp.png', 'image/png', 'public', 2011, '[]', '{\"uuid\":\"$2y$10$bjyIsZC0JXdIFT7AwVHvqO8CbXkfqCWGmPEkeMuwzy5I.ElGbXgGa\",\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 61, '2020-08-26 13:49:19', '2020-08-26 13:49:26'),
(63, 'App\\Models\\Upload', 31, 'default', '32', '32.jpg', 'image/jpeg', 'public', 31075, '[]', '{\"uuid\":\"45956460-d935-4420-aea0-1420b17b8e5d\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 62, '2020-10-19 15:07:07', '2020-10-19 15:07:15'),
(64, 'App\\Models\\Product', 4, 'image', 'Ideal plus', 'Ideal-plus.jpg', 'image/jpeg', 'public', 28482, '[]', '{\"uuid\":\"cfdf4fc3-4209-4fbb-98e5-91ec6ff0b7fa\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 63, '2020-10-19 15:10:25', '2020-10-19 15:10:25'),
(65, 'App\\Models\\Upload', 32, 'image', '01', '01.jpg', 'image/jpeg', 'public', 18862, '[]', '{\"uuid\":\"763ea2af-15c4-4ac3-b461-a40f9df4abe4\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 64, '2021-02-27 06:45:09', '2021-02-27 06:45:17'),
(67, 'App\\Models\\Upload', 33, 'default', '01', '01.jpg', 'image/jpeg', 'public', 246389, '[]', '{\"uuid\":\"7877ebf0-bc9b-4089-9eaa-10775414c941\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 65, '2021-02-27 06:49:02', '2021-02-27 06:49:05'),
(68, 'App\\Models\\Upload', 34, 'image', '09', '09.jpg', 'image/jpeg', 'public', 14947, '[]', '{\"uuid\":\"7efd4a3b-e9fd-4930-b6eb-e67065dd872e\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 66, '2021-02-27 06:55:05', '2021-02-27 06:55:07'),
(70, 'App\\Models\\Product', 1, 'image', 'Vin', 'Vin.jpg', 'image/jpeg', 'public', 6552, '[]', '{\"uuid\":\"25fe70e8-89cd-4248-adab-5ed3cc539245\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 67, '2021-02-27 06:57:20', '2021-02-27 06:57:20'),
(71, 'App\\Models\\Upload', 35, 'image', '09', '09.jpg', 'image/jpeg', 'public', 14947, '[]', '{\"uuid\":\"30155319-9d57-4d48-bdd8-a3a7a77e6117\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 68, '2021-02-28 21:28:23', '2021-02-28 21:28:31'),
(72, 'App\\Models\\Product', 2, 'image', '09', '09.jpg', 'image/jpeg', 'public', 14947, '[]', '{\"uuid\":\"30155319-9d57-4d48-bdd8-a3a7a77e6117\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 69, '2021-02-28 21:28:37', '2021-02-28 21:28:37'),
(73, 'App\\Models\\Upload', 36, 'image', '12', '12.jpg', 'image/jpeg', 'public', 16608, '[]', '{\"uuid\":\"621530b6-ed4a-479e-be57-c484f78f1d44\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 70, '2021-02-28 21:31:22', '2021-02-28 21:31:23'),
(74, 'App\\Models\\Product', 6, 'image', '12', '12.jpg', 'image/jpeg', 'public', 16608, '[]', '{\"uuid\":\"621530b6-ed4a-479e-be57-c484f78f1d44\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 71, '2021-02-28 21:32:25', '2021-02-28 21:32:25');

-- --------------------------------------------------------

--
-- Structure de la table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_05_26_175145_create_permission_tables', 1),
(4, '2018_06_12_140344_create_media_table', 1),
(5, '2018_06_13_035117_create_uploads_table', 1),
(6, '2018_07_17_180731_create_settings_table', 1),
(7, '2018_07_24_211308_create_custom_fields_table', 1),
(8, '2018_07_24_211327_create_custom_field_values_table', 1),
(9, '2019_08_29_213820_create_fields_table', 1),
(10, '2019_08_29_213821_create_markets_table', 1),
(11, '2019_08_29_213822_create_categories_table', 1),
(12, '2019_08_29_213826_create_option_groups_table', 1),
(13, '2019_08_29_213829_create_faq_categories_table', 1),
(14, '2019_08_29_213833_create_order_statuses_table', 1),
(15, '2019_08_29_213837_create_products_table', 1),
(16, '2019_08_29_213838_create_options_table', 1),
(17, '2019_08_29_213842_create_galleries_table', 1),
(18, '2019_08_29_213847_create_product_reviews_table', 1),
(19, '2019_08_29_213921_create_payments_table', 1),
(20, '2019_08_29_213922_create_delivery_addresses_table', 1),
(21, '2019_08_29_213926_create_faqs_table', 1),
(22, '2019_08_29_213940_create_market_reviews_table', 1),
(23, '2019_08_30_152927_create_favorites_table', 1),
(24, '2019_08_31_111104_create_orders_table', 1),
(25, '2019_09_04_153857_create_carts_table', 1),
(26, '2019_09_04_153858_create_favorite_options_table', 1),
(27, '2019_09_04_153859_create_cart_options_table', 1),
(28, '2019_09_04_153958_create_product_orders_table', 1),
(29, '2019_09_04_154957_create_product_order_options_table', 1),
(30, '2019_09_04_163857_create_user_markets_table', 1),
(31, '2019_10_22_144652_create_currencies_table', 1),
(32, '2019_12_14_134302_create_driver_markets_table', 1),
(33, '2020_03_25_094752_create_drivers_table', 1),
(34, '2020_03_25_094802_create_earnings_table', 1),
(35, '2020_03_25_094809_create_drivers_payouts_table', 1),
(36, '2020_03_25_094817_create_markets_payouts_table', 1),
(37, '2020_03_27_094855_create_notifications_table', 1),
(38, '2020_04_11_135804_create_market_fields_table', 1);

-- --------------------------------------------------------

--
-- Structure de la table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(2, 'App\\Models\\User', 1),
(2, 'App\\Models\\User', 7),
(3, 'App\\Models\\User', 2),
(4, 'App\\Models\\User', 3),
(4, 'App\\Models\\User', 4),
(4, 'App\\Models\\User', 8),
(4, 'App\\Models\\User', 9),
(4, 'App\\Models\\User', 10),
(4, 'App\\Models\\User', 11),
(4, 'App\\Models\\User', 12),
(5, 'App\\Models\\User', 5),
(5, 'App\\Models\\User', 6);

-- --------------------------------------------------------

--
-- Structure de la table `notifications`
--

CREATE TABLE `notifications` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` bigint(20) UNSIGNED NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `notifications`
--

INSERT INTO `notifications` (`id`, `type`, `notifiable_type`, `notifiable_id`, `data`, `read_at`, `created_at`, `updated_at`) VALUES
('60590d64-d628-484f-ad9a-b9659b98328f', 'App\\Notifications\\AssignedOrder', 'App\\Models\\User', 5, '{\"order_id\":1}', NULL, '2021-01-28 16:31:15', '2021-01-28 16:31:15');

-- --------------------------------------------------------

--
-- Structure de la table `options`
--

CREATE TABLE `options` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` double(8,2) NOT NULL DEFAULT 0.00,
  `product_id` int(10) UNSIGNED NOT NULL,
  `option_group_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `options`
--

INSERT INTO `options` (`id`, `name`, `description`, `price`, `product_id`, `option_group_id`, `created_at`, `updated_at`) VALUES
(3, '2L', 'Autem quia aut consequatur sunt.', 48.15, 4, 1, '2020-08-12 23:28:48', '2020-08-12 23:28:48'),
(6, 'Green', 'Officia eos et vel.', 25.99, 21, 4, '2020-08-12 23:28:49', '2020-08-12 23:28:49'),
(11, '500g', 'Impedit minima vel eius cupiditate.', 37.15, 28, 2, '2020-08-12 23:28:49', '2020-08-12 23:28:49'),
(15, '5L', 'Omnis in aspernatur blanditiis illum.', 49.73, 27, 3, '2020-08-12 23:28:50', '2020-08-12 23:28:50'),
(21, '5L', 'Cumque sunt quia unde vero.', 10.50, 6, 1, '2020-08-12 23:28:50', '2020-08-12 23:28:50'),
(23, '5L', 'Qui sint sint neque aperiam perspiciatis.', 38.84, 24, 1, '2020-08-12 23:28:50', '2020-08-12 23:28:50'),
(24, '2L', 'Velit asperiores ut ab.', 48.64, 1, 3, '2020-08-12 23:28:50', '2020-08-12 23:28:50'),
(36, 'XL', 'Mollitia temporibus eligendi animi.', 29.22, 1, 2, '2020-08-12 23:28:52', '2020-08-12 23:28:52'),
(39, 'Tomato', 'Aspernatur cum expedita modi repudiandae voluptas.', 24.02, 6, 1, '2020-08-12 23:28:52', '2020-08-12 23:28:52'),
(42, 'Oil', 'Rerum est enim illo aut.', 22.19, 18, 1, '2020-08-12 23:28:52', '2020-08-12 23:28:52'),
(43, 'White', 'Labore fuga laboriosam quia neque.', 22.23, 24, 4, '2020-08-12 23:28:52', '2020-08-12 23:28:52'),
(49, 'XL', 'Sed facilis accusantium et rerum ea.', 34.24, 18, 1, '2020-08-12 23:28:53', '2020-08-12 23:28:53'),
(52, 'L', 'Cumque enim quis iusto earum architecto.', 21.21, 28, 3, '2020-08-12 23:28:53', '2020-08-12 23:28:53'),
(53, '2L', 'Voluptas aut voluptates quia.', 32.12, 28, 3, '2020-08-12 23:28:53', '2020-08-12 23:28:53'),
(54, 'Oil', 'Architecto illo quaerat non.', 19.67, 21, 1, '2020-08-12 23:28:53', '2020-08-12 23:28:53'),
(56, 'L', 'Autem nobis quia laudantium suscipit.', 40.85, 24, 3, '2020-08-12 23:28:53', '2020-08-12 23:28:53'),
(60, '500g', 'Officiis numquam officia.', 18.70, 27, 3, '2020-08-12 23:28:53', '2020-08-12 23:28:53'),
(61, 'L', 'Voluptatem odio eligendi.', 20.66, 1, 2, '2020-08-12 23:28:53', '2020-08-12 23:28:53'),
(63, '500g', 'Dolores quia ut sunt.', 29.68, 6, 4, '2020-08-12 23:28:53', '2020-08-12 23:28:53'),
(66, 'Oil', 'Repudiandae molestiae non et iure.', 27.54, 4, 3, '2020-08-12 23:28:54', '2020-08-12 23:28:54');

-- --------------------------------------------------------

--
-- Structure de la table `option_groups`
--

CREATE TABLE `option_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `option_groups`
--

INSERT INTO `option_groups` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Size', '2019-08-31 10:55:28', '2019-08-31 10:55:28'),
(2, 'Color', '2019-10-09 13:26:28', '2019-10-09 13:26:28'),
(3, 'Parfum', '2019-10-09 13:26:28', '2019-10-09 13:26:28'),
(4, 'Taste', '2019-10-09 13:26:28', '2019-10-09 13:26:28');

-- --------------------------------------------------------

--
-- Structure de la table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `order_status_id` int(10) UNSIGNED NOT NULL,
  `tax` double(5,2) DEFAULT 0.00,
  `delivery_fee` double(5,2) DEFAULT 0.00,
  `hint` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `driver_id` int(10) UNSIGNED DEFAULT NULL,
  `delivery_address_id` int(10) UNSIGNED DEFAULT NULL,
  `payment_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `order_status_id`, `tax`, `delivery_fee`, `hint`, `active`, `driver_id`, `delivery_address_id`, `payment_id`, `created_at`, `updated_at`) VALUES
(1, 8, 1, 0.00, 0.00, NULL, 1, 5, NULL, 1, '2020-08-13 23:43:35', '2021-01-28 16:31:12');

-- --------------------------------------------------------

--
-- Structure de la table `order_statuses`
--

CREATE TABLE `order_statuses` (
  `id` int(10) UNSIGNED NOT NULL,
  `status` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `order_statuses`
--

INSERT INTO `order_statuses` (`id`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Order Received', '2019-08-30 16:39:28', '2019-10-15 18:03:14'),
(2, 'Preparing', '2019-10-15 18:03:50', '2019-10-15 18:03:50'),
(3, 'Ready', '2019-10-15 18:04:30', '2019-10-15 18:04:30'),
(4, 'On the Way', '2019-10-15 18:04:13', '2019-10-15 18:04:13'),
(5, 'Delivered', '2019-10-15 18:04:30', '2019-10-15 18:04:30');

-- --------------------------------------------------------

--
-- Structure de la table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `payments`
--

CREATE TABLE `payments` (
  `id` int(10) UNSIGNED NOT NULL,
  `price` double(8,2) NOT NULL DEFAULT 0.00,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `method` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `payments`
--

INSERT INTO `payments` (`id`, `price`, `description`, `user_id`, `status`, `method`, `created_at`, `updated_at`) VALUES
(1, 55.22, 'Order not paid yet', 8, 'Waiting for Client', 'Pay on Pickup', '2020-08-13 23:43:35', '2020-08-13 23:43:35');

-- --------------------------------------------------------

--
-- Structure de la table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'web',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'users.profile', 'web', '2020-03-29 14:58:02', '2020-03-29 14:58:02', NULL),
(2, 'dashboard', 'web', '2020-03-29 14:58:02', '2020-03-29 14:58:02', NULL),
(3, 'medias.create', 'web', '2020-03-29 14:58:02', '2020-03-29 14:58:02', NULL),
(4, 'medias.delete', 'web', '2020-03-29 14:58:02', '2020-03-29 14:58:02', NULL),
(5, 'medias', 'web', '2020-03-29 14:58:03', '2020-03-29 14:58:03', NULL),
(6, 'permissions.index', 'web', '2020-03-29 14:58:03', '2020-03-29 14:58:03', NULL),
(7, 'permissions.edit', 'web', '2020-03-29 14:58:03', '2020-03-29 14:58:03', NULL),
(8, 'permissions.update', 'web', '2020-03-29 14:58:03', '2020-03-29 14:58:03', NULL),
(9, 'permissions.destroy', 'web', '2020-03-29 14:58:03', '2020-03-29 14:58:03', NULL),
(10, 'roles.index', 'web', '2020-03-29 14:58:03', '2020-03-29 14:58:03', NULL),
(11, 'roles.edit', 'web', '2020-03-29 14:58:03', '2020-03-29 14:58:03', NULL),
(12, 'roles.update', 'web', '2020-03-29 14:58:03', '2020-03-29 14:58:03', NULL),
(13, 'roles.destroy', 'web', '2020-03-29 14:58:03', '2020-03-29 14:58:03', NULL),
(14, 'customFields.index', 'web', '2020-03-29 14:58:03', '2020-03-29 14:58:03', NULL),
(15, 'customFields.create', 'web', '2020-03-29 14:58:03', '2020-03-29 14:58:03', NULL),
(16, 'customFields.store', 'web', '2020-03-29 14:58:03', '2020-03-29 14:58:03', NULL),
(17, 'customFields.show', 'web', '2020-03-29 14:58:03', '2020-03-29 14:58:03', NULL),
(18, 'customFields.edit', 'web', '2020-03-29 14:58:03', '2020-03-29 14:58:03', NULL),
(19, 'customFields.update', 'web', '2020-03-29 14:58:04', '2020-03-29 14:58:04', NULL),
(20, 'customFields.destroy', 'web', '2020-03-29 14:58:04', '2020-03-29 14:58:04', NULL),
(21, 'users.login-as-user', 'web', '2020-03-29 14:58:04', '2020-03-29 14:58:04', NULL),
(22, 'users.index', 'web', '2020-03-29 14:58:04', '2020-03-29 14:58:04', NULL),
(23, 'users.create', 'web', '2020-03-29 14:58:04', '2020-03-29 14:58:04', NULL),
(24, 'users.store', 'web', '2020-03-29 14:58:04', '2020-03-29 14:58:04', NULL),
(25, 'users.show', 'web', '2020-03-29 14:58:04', '2020-03-29 14:58:04', NULL),
(26, 'users.edit', 'web', '2020-03-29 14:58:04', '2020-03-29 14:58:04', NULL),
(27, 'users.update', 'web', '2020-03-29 14:58:04', '2020-03-29 14:58:04', NULL),
(28, 'users.destroy', 'web', '2020-03-29 14:58:04', '2020-03-29 14:58:04', NULL),
(29, 'app-settings', 'web', '2020-03-29 14:58:04', '2020-03-29 14:58:04', NULL),
(30, 'markets.index', 'web', '2020-03-29 14:58:04', '2020-03-29 14:58:04', NULL),
(31, 'markets.create', 'web', '2020-03-29 14:58:04', '2020-03-29 14:58:04', NULL),
(32, 'markets.store', 'web', '2020-03-29 14:58:04', '2020-03-29 14:58:04', NULL),
(33, 'markets.edit', 'web', '2020-03-29 14:58:04', '2020-03-29 14:58:04', NULL),
(34, 'markets.update', 'web', '2020-03-29 14:58:05', '2020-03-29 14:58:05', NULL),
(35, 'markets.destroy', 'web', '2020-03-29 14:58:05', '2020-03-29 14:58:05', NULL),
(36, 'categories.index', 'web', '2020-03-29 14:58:05', '2020-03-29 14:58:05', NULL),
(37, 'categories.create', 'web', '2020-03-29 14:58:05', '2020-03-29 14:58:05', NULL),
(38, 'categories.store', 'web', '2020-03-29 14:58:05', '2020-03-29 14:58:05', NULL),
(39, 'categories.edit', 'web', '2020-03-29 14:58:05', '2020-03-29 14:58:05', NULL),
(40, 'categories.update', 'web', '2020-03-29 14:58:05', '2020-03-29 14:58:05', NULL),
(41, 'categories.destroy', 'web', '2020-03-29 14:58:05', '2020-03-29 14:58:05', NULL),
(42, 'faqCategories.index', 'web', '2020-03-29 14:58:06', '2020-03-29 14:58:06', NULL),
(43, 'faqCategories.create', 'web', '2020-03-29 14:58:06', '2020-03-29 14:58:06', NULL),
(44, 'faqCategories.store', 'web', '2020-03-29 14:58:06', '2020-03-29 14:58:06', NULL),
(45, 'faqCategories.edit', 'web', '2020-03-29 14:58:06', '2020-03-29 14:58:06', NULL),
(46, 'faqCategories.update', 'web', '2020-03-29 14:58:06', '2020-03-29 14:58:06', NULL),
(47, 'faqCategories.destroy', 'web', '2020-03-29 14:58:06', '2020-03-29 14:58:06', NULL),
(48, 'orderStatuses.index', 'web', '2020-03-29 14:58:06', '2020-03-29 14:58:06', NULL),
(49, 'orderStatuses.show', 'web', '2020-03-29 14:58:06', '2020-03-29 14:58:06', NULL),
(50, 'orderStatuses.edit', 'web', '2020-03-29 14:58:06', '2020-03-29 14:58:06', NULL),
(51, 'orderStatuses.update', 'web', '2020-03-29 14:58:07', '2020-03-29 14:58:07', NULL),
(52, 'products.index', 'web', '2020-03-29 14:58:07', '2020-03-29 14:58:07', NULL),
(53, 'products.create', 'web', '2020-03-29 14:58:07', '2020-03-29 14:58:07', NULL),
(54, 'products.store', 'web', '2020-03-29 14:58:07', '2020-03-29 14:58:07', NULL),
(55, 'products.edit', 'web', '2020-03-29 14:58:07', '2020-03-29 14:58:07', NULL),
(56, 'products.update', 'web', '2020-03-29 14:58:07', '2020-03-29 14:58:07', NULL),
(57, 'products.destroy', 'web', '2020-03-29 14:58:07', '2020-03-29 14:58:07', NULL),
(58, 'galleries.index', 'web', '2020-03-29 14:58:07', '2020-03-29 14:58:07', NULL),
(59, 'galleries.create', 'web', '2020-03-29 14:58:07', '2020-03-29 14:58:07', NULL),
(60, 'galleries.store', 'web', '2020-03-29 14:58:08', '2020-03-29 14:58:08', NULL),
(61, 'galleries.edit', 'web', '2020-03-29 14:58:08', '2020-03-29 14:58:08', NULL),
(62, 'galleries.update', 'web', '2020-03-29 14:58:08', '2020-03-29 14:58:08', NULL),
(63, 'galleries.destroy', 'web', '2020-03-29 14:58:08', '2020-03-29 14:58:08', NULL),
(64, 'productReviews.index', 'web', '2020-03-29 14:58:08', '2020-03-29 14:58:08', NULL),
(65, 'productReviews.create', 'web', '2020-03-29 14:58:08', '2020-03-29 14:58:08', NULL),
(66, 'productReviews.store', 'web', '2020-03-29 14:58:08', '2020-03-29 14:58:08', NULL),
(67, 'productReviews.edit', 'web', '2020-03-29 14:58:08', '2020-03-29 14:58:08', NULL),
(68, 'productReviews.update', 'web', '2020-03-29 14:58:08', '2020-03-29 14:58:08', NULL),
(69, 'productReviews.destroy', 'web', '2020-03-29 14:58:08', '2020-03-29 14:58:08', NULL),
(76, 'options.index', 'web', '2020-03-29 14:58:09', '2020-03-29 14:58:09', NULL),
(77, 'options.create', 'web', '2020-03-29 14:58:09', '2020-03-29 14:58:09', NULL),
(78, 'options.store', 'web', '2020-03-29 14:58:09', '2020-03-29 14:58:09', NULL),
(79, 'options.show', 'web', '2020-03-29 14:58:10', '2020-03-29 14:58:10', NULL),
(80, 'options.edit', 'web', '2020-03-29 14:58:10', '2020-03-29 14:58:10', NULL),
(81, 'options.update', 'web', '2020-03-29 14:58:10', '2020-03-29 14:58:10', NULL),
(82, 'options.destroy', 'web', '2020-03-29 14:58:10', '2020-03-29 14:58:10', NULL),
(83, 'payments.index', 'web', '2020-03-29 14:58:10', '2020-03-29 14:58:10', NULL),
(84, 'payments.show', 'web', '2020-03-29 14:58:10', '2020-03-29 14:58:10', NULL),
(85, 'payments.update', 'web', '2020-03-29 14:58:10', '2020-03-29 14:58:10', NULL),
(86, 'faqs.index', 'web', '2020-03-29 14:58:10', '2020-03-29 14:58:10', NULL),
(87, 'faqs.create', 'web', '2020-03-29 14:58:11', '2020-03-29 14:58:11', NULL),
(88, 'faqs.store', 'web', '2020-03-29 14:58:11', '2020-03-29 14:58:11', NULL),
(89, 'faqs.edit', 'web', '2020-03-29 14:58:11', '2020-03-29 14:58:11', NULL),
(90, 'faqs.update', 'web', '2020-03-29 14:58:11', '2020-03-29 14:58:11', NULL),
(91, 'faqs.destroy', 'web', '2020-03-29 14:58:11', '2020-03-29 14:58:11', NULL),
(92, 'marketReviews.index', 'web', '2020-03-29 14:58:11', '2020-03-29 14:58:11', NULL),
(93, 'marketReviews.create', 'web', '2020-03-29 14:58:11', '2020-03-29 14:58:11', NULL),
(94, 'marketReviews.store', 'web', '2020-03-29 14:58:12', '2020-03-29 14:58:12', NULL),
(95, 'marketReviews.edit', 'web', '2020-03-29 14:58:12', '2020-03-29 14:58:12', NULL),
(96, 'marketReviews.update', 'web', '2020-03-29 14:58:12', '2020-03-29 14:58:12', NULL),
(97, 'marketReviews.destroy', 'web', '2020-03-29 14:58:12', '2020-03-29 14:58:12', NULL),
(98, 'favorites.index', 'web', '2020-03-29 14:58:12', '2020-03-29 14:58:12', NULL),
(99, 'favorites.create', 'web', '2020-03-29 14:58:12', '2020-03-29 14:58:12', NULL),
(100, 'favorites.store', 'web', '2020-03-29 14:58:12', '2020-03-29 14:58:12', NULL),
(101, 'favorites.edit', 'web', '2020-03-29 14:58:12', '2020-03-29 14:58:12', NULL),
(102, 'favorites.update', 'web', '2020-03-29 14:58:12', '2020-03-29 14:58:12', NULL),
(103, 'favorites.destroy', 'web', '2020-03-29 14:58:13', '2020-03-29 14:58:13', NULL),
(104, 'orders.index', 'web', '2020-03-29 14:58:13', '2020-03-29 14:58:13', NULL),
(105, 'orders.create', 'web', '2020-03-29 14:58:13', '2020-03-29 14:58:13', NULL),
(106, 'orders.store', 'web', '2020-03-29 14:58:13', '2020-03-29 14:58:13', NULL),
(107, 'orders.show', 'web', '2020-03-29 14:58:13', '2020-03-29 14:58:13', NULL),
(108, 'orders.edit', 'web', '2020-03-29 14:58:13', '2020-03-29 14:58:13', NULL),
(109, 'orders.update', 'web', '2020-03-29 14:58:13', '2020-03-29 14:58:13', NULL),
(110, 'orders.destroy', 'web', '2020-03-29 14:58:13', '2020-03-29 14:58:13', NULL),
(111, 'notifications.index', 'web', '2020-03-29 14:58:14', '2020-03-29 14:58:14', NULL),
(112, 'notifications.show', 'web', '2020-03-29 14:58:14', '2020-03-29 14:58:14', NULL),
(113, 'notifications.destroy', 'web', '2020-03-29 14:58:14', '2020-03-29 14:58:14', NULL),
(114, 'carts.index', 'web', '2020-03-29 14:58:14', '2020-03-29 14:58:14', NULL),
(115, 'carts.edit', 'web', '2020-03-29 14:58:14', '2020-03-29 14:58:14', NULL),
(116, 'carts.update', 'web', '2020-03-29 14:58:14', '2020-03-29 14:58:14', NULL),
(117, 'carts.destroy', 'web', '2020-03-29 14:58:14', '2020-03-29 14:58:14', NULL),
(118, 'currencies.index', 'web', '2020-03-29 14:58:14', '2020-03-29 14:58:14', NULL),
(119, 'currencies.create', 'web', '2020-03-29 14:58:15', '2020-03-29 14:58:15', NULL),
(120, 'currencies.store', 'web', '2020-03-29 14:58:15', '2020-03-29 14:58:15', NULL),
(121, 'currencies.edit', 'web', '2020-03-29 14:58:15', '2020-03-29 14:58:15', NULL),
(122, 'currencies.update', 'web', '2020-03-29 14:58:15', '2020-03-29 14:58:15', NULL),
(123, 'currencies.destroy', 'web', '2020-03-29 14:58:15', '2020-03-29 14:58:15', NULL),
(124, 'deliveryAddresses.index', 'web', '2020-03-29 14:58:15', '2020-03-29 14:58:15', NULL),
(125, 'deliveryAddresses.create', 'web', '2020-03-29 14:58:15', '2020-03-29 14:58:15', NULL),
(126, 'deliveryAddresses.store', 'web', '2020-03-29 14:58:15', '2020-03-29 14:58:15', NULL),
(127, 'deliveryAddresses.edit', 'web', '2020-03-29 14:58:16', '2020-03-29 14:58:16', NULL),
(128, 'deliveryAddresses.update', 'web', '2020-03-29 14:58:16', '2020-03-29 14:58:16', NULL),
(129, 'deliveryAddresses.destroy', 'web', '2020-03-29 14:58:16', '2020-03-29 14:58:16', NULL),
(130, 'drivers.index', 'web', '2020-03-29 14:58:16', '2020-03-29 14:58:16', NULL),
(131, 'drivers.create', 'web', '2020-03-29 14:58:16', '2020-03-29 14:58:16', NULL),
(132, 'drivers.store', 'web', '2020-03-29 14:58:16', '2020-03-29 14:58:16', NULL),
(133, 'drivers.show', 'web', '2020-03-29 14:58:16', '2020-03-29 14:58:16', NULL),
(134, 'drivers.edit', 'web', '2020-03-29 14:58:16', '2020-03-29 14:58:16', NULL),
(135, 'drivers.update', 'web', '2020-03-29 14:58:16', '2020-03-29 14:58:16', NULL),
(136, 'drivers.destroy', 'web', '2020-03-29 14:58:17', '2020-03-29 14:58:17', NULL),
(137, 'earnings.index', 'web', '2020-03-29 14:58:17', '2020-03-29 14:58:17', NULL),
(138, 'earnings.create', 'web', '2020-03-29 14:58:17', '2020-03-29 14:58:17', NULL),
(139, 'earnings.store', 'web', '2020-03-29 14:58:17', '2020-03-29 14:58:17', NULL),
(140, 'earnings.show', 'web', '2020-03-29 14:58:17', '2020-03-29 14:58:17', NULL),
(141, 'earnings.edit', 'web', '2020-03-29 14:58:17', '2020-03-29 14:58:17', NULL),
(142, 'earnings.update', 'web', '2020-03-29 14:58:17', '2020-03-29 14:58:17', NULL),
(143, 'earnings.destroy', 'web', '2020-03-29 14:58:17', '2020-03-29 14:58:17', NULL),
(144, 'driversPayouts.index', 'web', '2020-03-29 14:58:17', '2020-03-29 14:58:17', NULL),
(145, 'driversPayouts.create', 'web', '2020-03-29 14:58:17', '2020-03-29 14:58:17', NULL),
(146, 'driversPayouts.store', 'web', '2020-03-29 14:58:18', '2020-03-29 14:58:18', NULL),
(147, 'driversPayouts.show', 'web', '2020-03-29 14:58:18', '2020-03-29 14:58:18', NULL),
(148, 'driversPayouts.edit', 'web', '2020-03-29 14:58:18', '2020-03-29 14:58:18', NULL),
(149, 'driversPayouts.update', 'web', '2020-03-29 14:58:18', '2020-03-29 14:58:18', NULL),
(150, 'driversPayouts.destroy', 'web', '2020-03-29 14:58:18', '2020-03-29 14:58:18', NULL),
(151, 'marketsPayouts.index', 'web', '2020-03-29 14:58:18', '2020-03-29 14:58:18', NULL),
(152, 'marketsPayouts.create', 'web', '2020-03-29 14:58:18', '2020-03-29 14:58:18', NULL),
(153, 'marketsPayouts.store', 'web', '2020-03-29 14:58:18', '2020-03-29 14:58:18', NULL),
(154, 'marketsPayouts.show', 'web', '2020-03-29 14:58:18', '2020-03-29 14:58:18', NULL),
(155, 'marketsPayouts.edit', 'web', '2020-03-29 14:58:18', '2020-03-29 14:58:18', NULL),
(156, 'marketsPayouts.update', 'web', '2020-03-29 14:58:19', '2020-03-29 14:58:19', NULL),
(157, 'marketsPayouts.destroy', 'web', '2020-03-29 14:58:19', '2020-03-29 14:58:19', NULL),
(158, 'permissions.create', 'web', '2020-03-29 14:59:15', '2020-03-29 14:59:15', NULL),
(159, 'permissions.store', 'web', '2020-03-29 14:59:15', '2020-03-29 14:59:15', NULL),
(160, 'permissions.show', 'web', '2020-03-29 14:59:15', '2020-03-29 14:59:15', NULL),
(161, 'roles.create', 'web', '2020-03-29 14:59:15', '2020-03-29 14:59:15', NULL),
(162, 'roles.store', 'web', '2020-03-29 14:59:15', '2020-03-29 14:59:15', NULL),
(163, 'roles.show', 'web', '2020-03-29 14:59:16', '2020-03-29 14:59:16', NULL),
(164, 'fields.index', 'web', '2020-04-11 15:04:39', '2020-04-11 15:04:39', NULL),
(165, 'fields.create', 'web', '2020-04-11 15:04:39', '2020-04-11 15:04:39', NULL),
(166, 'fields.store', 'web', '2020-04-11 15:04:39', '2020-04-11 15:04:39', NULL),
(167, 'fields.edit', 'web', '2020-04-11 15:04:39', '2020-04-11 15:04:39', NULL),
(168, 'fields.update', 'web', '2020-04-11 15:04:39', '2020-04-11 15:04:39', NULL),
(169, 'fields.destroy', 'web', '2020-04-11 15:04:40', '2020-04-11 15:04:40', NULL),
(170, 'optionGroups.index', 'web', '2020-04-11 15:04:40', '2020-04-11 15:04:40', NULL),
(171, 'optionGroups.create', 'web', '2020-04-11 15:04:40', '2020-04-11 15:04:40', NULL),
(172, 'optionGroups.store', 'web', '2020-04-11 15:04:40', '2020-04-11 15:04:40', NULL),
(173, 'optionGroups.edit', 'web', '2020-04-11 15:04:40', '2020-04-11 15:04:40', NULL),
(174, 'optionGroups.update', 'web', '2020-04-11 15:04:40', '2020-04-11 15:04:40', NULL),
(175, 'optionGroups.destroy', 'web', '2020-04-11 15:04:40', '2020-04-11 15:04:40', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(8,2) NOT NULL DEFAULT 0.00,
  `discount_price` double(8,2) DEFAULT 0.00,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `capacity` double(9,2) DEFAULT 0.00,
  `package_items_count` double(9,2) DEFAULT 0.00,
  `unit` varchar(127) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `featured` tinyint(1) DEFAULT 0,
  `deliverable` tinyint(1) DEFAULT 1,
  `market_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `products`
--

INSERT INTO `products` (`id`, `name`, `price`, `discount_price`, `description`, `capacity`, `package_items_count`, `unit`, `featured`, `deliverable`, `market_id`, `category_id`, `created_at`, `updated_at`) VALUES
(2, 'Alexander the great', 150000.00, 8000.00, 'Test', 750.00, 10.00, 'ml', 1, 0, 1, 2, '2021-02-28 20:25:00', '2021-02-28 21:19:02'),
(4, 'Vallpierre', 2100.00, 2000.00, '<p>Test<br></p>', 10.00, 10.00, '1000ml', 0, 1, 1, 2, '2021-02-28 21:01:47', '2021-02-28 21:18:35'),
(5, 'Saint Pièrre', 3500.00, 2000.00, '<p>Test<br></p>', 10.00, 15.00, 'ml', 0, 0, 4, 2, '2021-02-28 21:17:49', '2021-02-28 21:18:10'),
(6, 'Test', 100000.00, 10000.00, '<p>test<br></p>', 10.00, 10.00, 'ml', 0, 0, 4, 2, '2021-02-28 21:32:24', '2021-02-28 21:32:24');

-- --------------------------------------------------------

--
-- Structure de la table `product_orders`
--

CREATE TABLE `product_orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `price` double(8,2) NOT NULL DEFAULT 0.00,
  `quantity` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `product_id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `product_orders`
--

INSERT INTO `product_orders` (`id`, `price`, `quantity`, `product_id`, `order_id`, `created_at`, `updated_at`) VALUES
(1, 35.59, 1, 28, 1, '2020-08-13 23:43:35', '2020-08-13 23:43:35'),
(2, 15.52, 1, 4, 1, '2020-08-13 23:43:35', '2020-08-13 23:43:35');

-- --------------------------------------------------------

--
-- Structure de la table `product_order_options`
--

CREATE TABLE `product_order_options` (
  `product_order_id` int(10) UNSIGNED NOT NULL,
  `option_id` int(10) UNSIGNED NOT NULL,
  `price` double(8,2) NOT NULL DEFAULT 0.00
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `product_reviews`
--

CREATE TABLE `product_reviews` (
  `id` int(10) UNSIGNED NOT NULL,
  `review` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rate` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `user_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `product_reviews`
--

INSERT INTO `product_reviews` (`id`, `review`, `rate`, `user_id`, `product_id`, `created_at`, `updated_at`) VALUES
(11, 'Off with his head!\"\' \'How dreadfully savage!\' exclaimed Alice. \'And ever since that,\' the Hatter continued, \'in this way:-- \"Up above the world you fly, Like a tea-tray in the middle. Alice kept her.', 3, 2, 6, '2020-08-12 23:28:43', '2020-08-12 23:28:43'),
(12, 'I like being that person, I\'ll come up: if not, I\'ll stay down here! It\'ll be no sort of way, \'Do cats eat bats?\' and sometimes, \'Do bats eat cats?\' for, you see, Miss, this here ought to go down.', 2, 6, 21, '2020-08-12 23:28:43', '2020-08-12 23:28:43'),
(19, 'I like\"!\' \'You might just as I was going on between the executioner, the King, and he poured a little recovered from the Queen left off, quite out of the jurymen. \'No, they\'re not,\' said the.', 2, 5, 21, '2020-08-12 23:28:44', '2020-08-12 23:28:44'),
(23, 'And certainly there was hardly room to grow to my right size to do THAT in a thick wood. \'The first thing she heard one of the other side, the puppy jumped into the book her sister was reading, but.', 2, 2, 24, '2020-08-12 23:28:44', '2020-08-12 23:28:44'),
(24, 'I will prosecute YOU.--Come, I\'ll take no denial; We must have imitated somebody else\'s hand,\' said the Duchess, \'as pigs have to go on crying in this affair, He trusts to you how the Dodo solemnly.', 3, 4, 4, '2020-08-12 23:28:44', '2020-08-12 23:28:44'),
(27, 'Dormouse. \'Fourteenth of March, I think I can creep under the window, and one foot up the conversation a little. \'\'Tis so,\' said the Queen, turning purple. \'I won\'t!\' said Alice. \'Who\'s making.', 3, 5, 24, '2020-08-12 23:28:44', '2020-08-12 23:28:44'),
(30, 'Hatter, it woke up again with a sudden leap out of its little eyes, but it was out of sight: then it chuckled. \'What fun!\' said the Cat went on, \'if you don\'t even know what to do, and perhaps as.', 4, 4, 6, '2020-08-12 23:28:45', '2020-08-12 23:28:45');

-- --------------------------------------------------------

--
-- Structure de la table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'web',
  `default` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `default`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 'admin', 'web', 0, '2018-07-21 16:37:56', '2019-09-07 22:42:01', NULL),
(3, 'manager', 'web', 0, '2019-09-07 22:41:38', '2019-09-07 22:41:38', NULL),
(4, 'client', 'web', 1, '2019-09-07 22:41:54', '2019-09-07 22:41:54', NULL),
(5, 'driver', 'web', 0, '2019-12-15 18:50:21', '2019-12-15 18:50:21', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(2, 2),
(3, 2),
(3, 3),
(4, 2),
(4, 3),
(5, 2),
(5, 3),
(6, 2),
(9, 2),
(10, 2),
(14, 2),
(15, 2),
(16, 2),
(17, 2),
(18, 2),
(19, 2),
(20, 2),
(21, 2),
(22, 2),
(23, 2),
(24, 2),
(26, 2),
(27, 2),
(27, 3),
(27, 4),
(27, 5),
(28, 2),
(29, 2),
(30, 2),
(30, 3),
(30, 4),
(30, 5),
(31, 2),
(32, 2),
(33, 2),
(33, 3),
(34, 2),
(34, 3),
(35, 2),
(36, 2),
(37, 2),
(38, 2),
(39, 2),
(40, 2),
(41, 2),
(42, 2),
(42, 3),
(43, 2),
(44, 2),
(45, 2),
(46, 2),
(47, 2),
(48, 2),
(48, 3),
(48, 5),
(50, 2),
(51, 2),
(52, 2),
(52, 3),
(52, 4),
(52, 5),
(53, 2),
(53, 3),
(54, 2),
(54, 3),
(55, 2),
(55, 3),
(56, 2),
(56, 3),
(57, 2),
(57, 3),
(58, 2),
(58, 3),
(59, 2),
(59, 3),
(60, 2),
(60, 3),
(61, 2),
(61, 3),
(62, 2),
(62, 3),
(63, 2),
(63, 3),
(64, 2),
(64, 3),
(64, 4),
(64, 5),
(67, 2),
(67, 3),
(67, 4),
(67, 5),
(68, 2),
(68, 3),
(68, 4),
(68, 5),
(69, 2),
(76, 2),
(76, 3),
(77, 2),
(77, 3),
(78, 2),
(78, 3),
(80, 2),
(80, 3),
(81, 2),
(81, 3),
(82, 2),
(82, 3),
(83, 2),
(83, 3),
(83, 4),
(83, 5),
(85, 2),
(86, 2),
(86, 3),
(86, 4),
(86, 5),
(87, 2),
(88, 2),
(89, 2),
(90, 2),
(91, 2),
(92, 2),
(92, 3),
(92, 4),
(92, 5),
(95, 2),
(95, 3),
(95, 4),
(95, 5),
(96, 2),
(96, 3),
(96, 4),
(96, 5),
(97, 2),
(98, 2),
(98, 3),
(98, 4),
(98, 5),
(103, 2),
(103, 3),
(103, 4),
(103, 5),
(104, 2),
(104, 3),
(104, 4),
(104, 5),
(107, 2),
(107, 3),
(107, 4),
(107, 5),
(108, 2),
(108, 3),
(109, 2),
(109, 3),
(110, 2),
(110, 3),
(111, 2),
(111, 3),
(111, 4),
(111, 5),
(112, 2),
(113, 2),
(113, 3),
(113, 4),
(113, 5),
(114, 2),
(114, 3),
(114, 4),
(114, 5),
(117, 2),
(117, 3),
(117, 4),
(117, 5),
(118, 2),
(119, 2),
(120, 2),
(121, 2),
(122, 2),
(123, 2),
(124, 2),
(127, 2),
(128, 2),
(129, 2),
(130, 2),
(130, 3),
(130, 5),
(131, 2),
(134, 2),
(134, 3),
(135, 2),
(135, 3),
(137, 2),
(137, 3),
(138, 2),
(144, 2),
(144, 5),
(145, 2),
(145, 3),
(145, 5),
(146, 2),
(146, 3),
(146, 5),
(148, 2),
(149, 2),
(151, 2),
(151, 3),
(152, 2),
(152, 3),
(153, 2),
(153, 3),
(155, 2),
(156, 2),
(160, 2),
(164, 2),
(164, 3),
(164, 4),
(164, 5),
(165, 2),
(166, 2),
(167, 2),
(168, 2),
(169, 2),
(170, 2),
(170, 3),
(171, 2),
(171, 3),
(172, 2),
(172, 3),
(173, 2),
(174, 2),
(175, 2);

-- --------------------------------------------------------

--
-- Structure de la table `uploads`
--

CREATE TABLE `uploads` (
  `id` int(10) UNSIGNED NOT NULL,
  `uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `uploads`
--

INSERT INTO `uploads` (`id`, `uuid`, `created_at`, `updated_at`) VALUES
(1, '6e5c6acd-44f8-4669-bfd5-2098eae62d94', '2020-08-13 19:19:54', '2020-08-13 19:19:54'),
(2, 'e095af18-01fd-46a0-a153-090847a7900e', '2020-08-13 23:07:15', '2020-08-13 23:07:15'),
(3, 'ca1e4b11-5170-45d5-828a-1f5948ea9190', '2020-08-13 23:08:14', '2020-08-13 23:08:14'),
(4, '3d752b46-5d01-46cb-97f7-1b0b2b67b632', '2020-08-13 23:09:48', '2020-08-13 23:09:48'),
(5, 'de745709-937e-44ad-bb95-bd3d5809e2f3', '2020-08-13 23:10:41', '2020-08-13 23:10:41'),
(6, '6b5fc961-d625-4aa7-8de5-93a6450fa1bf', '2020-08-13 23:11:14', '2020-08-13 23:11:14'),
(7, 'aa32f3be-a8f5-4b37-a230-9370cb47d9af', '2020-08-14 21:37:20', '2020-08-14 21:37:20'),
(8, '16d2471d-4aba-4b4a-8131-c3d8ff0824ec', '2020-08-16 22:22:03', '2020-08-16 22:22:03'),
(9, '56ea51ac-e9ad-4cae-a692-184504dbef02', '2020-08-16 22:29:25', '2020-08-16 22:29:25'),
(10, 'e9996c8d-09e1-4573-bd59-e0dc1b864b01', '2020-08-16 22:30:22', '2020-08-16 22:30:22'),
(11, 'dd46b237-c84b-409b-aaa9-2ab2d81edac9', '2020-08-16 22:32:09', '2020-08-16 22:32:09'),
(12, '8f1467bc-c530-4b66-bb8f-15206a1773da', '2020-08-16 22:47:34', '2020-08-16 22:47:34'),
(13, '25fe70e8-89cd-4248-adab-5ed3cc539245', '2020-08-16 22:49:27', '2020-08-16 22:49:27'),
(14, '522f38d0-15d3-48d5-b169-0b8f87ece256', '2020-08-16 22:52:29', '2020-08-16 22:52:29'),
(15, 'e8811090-8db5-4dff-b663-12b8ea07756b', '2020-08-16 23:01:46', '2020-08-16 23:01:46'),
(16, 'e7c43876-e65b-4964-88c3-52d6cafcee91', '2020-08-16 23:02:34', '2020-08-16 23:02:34'),
(17, '183cd04b-6048-4818-8b80-7fd21f269b3d', '2020-08-16 23:04:48', '2020-08-16 23:04:48'),
(18, '4fd23d06-508b-4be7-bc54-26dc5e8e0b12', '2020-08-16 23:15:08', '2020-08-16 23:15:08'),
(19, 'f11ff95f-629a-4688-91ad-3631a8dd8432', '2020-08-16 23:27:22', '2020-08-16 23:27:22'),
(20, '30492009-2b64-4b98-8930-95e59c901bb1', '2020-08-16 23:38:50', '2020-08-16 23:38:50'),
(21, 'b74217c0-9136-4262-be8c-79119bc206ba', '2020-08-16 23:43:41', '2020-08-16 23:43:41'),
(22, '3537a0c6-cb68-43a5-bcf1-87eb9472a353', '2020-08-16 23:45:18', '2020-08-16 23:45:18'),
(23, 'bce0f055-31bd-4711-9878-f92a269ffbb1', '2020-08-16 23:55:39', '2020-08-16 23:55:39'),
(24, '6b761dac-feeb-4e94-9113-f518c4c3d91f', '2020-08-19 14:11:26', '2020-08-19 14:11:26'),
(25, '3dd8e32b-70fb-48fe-b3b7-e75ee107add3', '2020-08-19 14:14:56', '2020-08-19 14:14:56'),
(26, '6a329678-304e-46f5-90b9-b5a1d4b2cf89', '2020-08-19 14:18:33', '2020-08-19 14:18:33'),
(27, 'db174ff1-64ff-4812-aeb8-005afc5d04e7', '2020-08-19 14:20:11', '2020-08-19 14:20:11'),
(28, 'd901f912-e981-4c42-bc70-2a1aa935daa0', '2020-08-19 14:21:18', '2020-08-19 14:21:18'),
(29, '093a030e-d496-4edd-81d7-966161fb560c', '2020-08-19 14:25:26', '2020-08-19 14:25:26'),
(30, 'cfdf4fc3-4209-4fbb-98e5-91ec6ff0b7fa', '2020-08-19 14:27:02', '2020-08-19 14:27:02'),
(31, '45956460-d935-4420-aea0-1420b17b8e5d', '2020-10-19 15:07:07', '2020-10-19 15:07:07'),
(32, '763ea2af-15c4-4ac3-b461-a40f9df4abe4', '2021-02-27 06:45:08', '2021-02-27 06:45:08'),
(33, '7877ebf0-bc9b-4089-9eaa-10775414c941', '2021-02-27 06:49:02', '2021-02-27 06:49:02'),
(34, '7efd4a3b-e9fd-4930-b6eb-e67065dd872e', '2021-02-27 06:55:05', '2021-02-27 06:55:05'),
(35, '30155319-9d57-4d48-bdd8-a3a7a77e6117', '2021-02-28 21:28:22', '2021-02-28 21:28:22'),
(36, '621530b6-ed4a-479e-be57-c484f78f1d44', '2021-02-28 21:31:22', '2021-02-28 21:31:22');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `api_token` char(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `device_token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stripe_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_brand` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_last_four` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trial_ends_at` timestamp NULL DEFAULT NULL,
  `braintree_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paypal_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `api_token`, `device_token`, `stripe_id`, `card_brand`, `card_last_four`, `trial_ends_at`, `braintree_id`, `paypal_email`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'John Doe', 'admin@demo.com', '$2y$10$YOn/Xq6vfvi9oaixrtW8QuM2W0mawkLLqIxL.IoGqrsqOqbIsfBNu', 'PivvPlsQWxPl1bB5KrbKNBuraJit0PrUZekQUgtLyTRuyBq921atFtoR1HuA', 'dU7kqqKJQk6FgPnCRFS9So:APA91bFmCWS0DLPuqDV5nah0pt7112BFEkf0dWYXK-SEyEFIy_w-gXUNS_RdCNQlfKkYTMynRzapdZdHHc53briVEZ3Mn5pIRUQZW6anNU-_HDkqY2mgplEsLI7MQ1TnRVVOywFe63AG', NULL, NULL, NULL, NULL, NULL, NULL, '0l9YeruxLbSQH3OaYcoJD4HJoIbw5sJhFBmTZIqGxtI9cmux7IIEfWVbtP6M', '2018-08-06 22:58:41', '2020-09-27 15:58:16'),
(2, 'Barbara J. Glanz', 'manager@demo.com', '$2y$10$YOn/Xq6vfvi9oaixrtW8QuM2W0mawkLLqIxL.IoGqrsqOqbIsfBNu', 'tVSfIKRSX2Yn8iAMoUS3HPls84ycS8NAxO2dj2HvePbbr4WHorp4gIFRmFwB', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '5nysjzVKI4LU92bjRqMUSYdOaIo1EcPC3pIMb6Tcj2KXSUMriGrIQ1iwRdd0', '2018-08-14 17:06:28', '2019-09-25 22:09:35'),
(3, 'Charles W. Abeyta', 'client@demo.com', '$2y$10$EBubVy3wDbqNbHvMQwkj3OTYVitL8QnHvh/zV0ICVOaSbALy5dD0K', 'fXLu7VeYgXDu82SkMxlLPG1mCAXc4EBIx6O5isgYVIKFQiHah0xiOHmzNsBv', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'V6PIUfd8JdHT2zkraTlnBcRSINZNjz5Ou7N0WtUGRyaTweoaXKpSfij6UhqC', '2019-10-12 22:31:26', '2020-03-29 17:44:30'),
(4, 'Robert E. Brock', 'client1@demo.com', '$2y$10$pmdnepS1FhZUMqOaFIFnNO0spltJpziz3j13UqyEwShmLhokmuoei', 'Czrsk9rwD0c75NUPkzNXM2WvbxYHKj8p0nG29pjKT0PZaTgMVzuVyv4hOlte', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-10-15 17:55:39', '2020-03-29 17:59:39'),
(5, 'Sanchez Roberto', 'driver@demo.com', '$2y$10$T/jwzYDJfC8c9CdD5PbpuOKvEXlpv4.RR1jMT0PgIMT.fzeGw67JO', 'OuMsmU903WMcMhzAbuSFtxBekZVdXz66afifRo3YRCINi38jkXJ8rpN0FcfS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-15 18:49:44', '2020-03-29 17:22:10'),
(6, 'John Doe', 'driver1@demo.com', '$2y$10$YF0jCx2WCQtfZOq99hR8kuXsAE0KSnu5OYSomRtI9iCVguXDoDqVm', 'zh9mzfNO2iPtIxj6k4Jpj8flaDyOsxmlGRVUZRnJqOGBr8IuDyhb3cGoncvS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-29 17:28:04', '2020-03-29 17:28:04'),
(7, 'test', 'ultrondev@gmail.com', '$2y$10$n1jTwr349enbDnzbuY4UfOZQi7APXwkcBE9hFjjw8CRzD63z0rLrS', 'KHjewvtDHltCLVp7MHnN4Tinm4zmvxXAWs1Y1O0RfYM1P0shxPHAtfO4dbGb', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'MqUljVzGYmOv88aa0C1yvbCeeVqkyJaKSw05Vg0h1HojlU7UyG3IKYks6jHE', '2020-08-13 11:52:21', '2020-08-13 11:52:21'),
(8, 'Jane Doe', 'grebejordan@gmail.com', '$2y$10$GeEZr4de5Rua8hej4FsBPem.Ouk/SwNig4U/J1Y36ZBMFJtXalxaa', 'O4EZoiMhE5RbPgsZK51BLx62PYuE6yxDuyY2tONwjbkuv1M3pnTrYgglaOi1', 'fC2No2R_Rd-Tp1DRX_0iHT:APA91bGFq5tukBlJuuX31mNK6vhZGYz0a9r0gAPUmaXGHR75ML66FMQ5FfTY1SnELQcHe9a9yEAGabtODjRxpfU5Me9ywZVc_cc1eN-L88jQbb5Hh0lYJjHU6aHjLzKOAKDscWfF2GdK', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-13 23:36:34', '2020-08-13 23:36:34'),
(9, 'Becanty', 'arthurb.kouame@gmail.com', '$2y$10$BgV4pkXds35htaPAYcXBWeHpdFPMXIT0/kORFU8/tLIRieJVytrZe', 'mWqJou5phUBFypI6NKW4c3sN4JCjoyxgnNurdKq0FSua75UIkCC8QbS7695E', 'fJFUNHsbQP2BJC8fZd16zW:APA91bEwnES6XSbGY2Dh0Xb-_6WKxSvO0PYi0YXc3n5hA9XArUcH6zxeUdXqa7Iu8jRSf1xel4HC9emDW90KBGH0fuzMBUY0OPYuiyr3mYFVpYUrxvF7kk8PNEPa_Vx9anFpyvYCYwT5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-14 07:36:50', '2020-08-14 07:36:50'),
(10, 'test', 'test@test.com', '$2y$10$MKlor3xyhVw13tFOxStOKuSqvhn2f4Zqp3LXSNfyTTHxJ98apKkGC', 'tubKSqQVjTdWiG95wuIRX7ObERaM63p9aJEc3kOUZDaXT3vKBaJOgScioeiu', 'elDXkSgKQ7W38FFQVg0KxQ:APA91bFKnRdJS5q8NdLzMHr37_MWfVFgOcG9PTBLX6W6yig6tuPe8irJiWIsQRbNVkd1F2-omScO-vVireXMqOn80RTISKCH6TAnzSedq5dPxKdAj_vGz13kDcRYZTr1aw1U5C3JaUQR', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-14 17:33:08', '2020-08-14 17:33:08'),
(11, 'Donantcha KOUASSI', 'donantchaig2016@gmail.com', '$2y$10$A8ABbZ/tCmtDH6KHr1T24OZwXBn.uNLosM1qE8GRPH/e2dsZclEMq', 'Fr4w2VaF9ZwYohYAzKo92FPt48wNO6D0lGZuAQJPHvSpg3TRDEHEA0oVPALL', 'dfKEfNsRRBSo_-kNuR-sJH:APA91bGcwVn_9CUVtWLOvgHk4wn36LkVeC2H3aTx9zpZrBu_BVpPx_usMrvku5ZongRbJz8cneBfe3JnqxcuFrMN_FztTpIcbk6EHSHvvU04wBZziT9OrcZZ6kd6ZCbcqk8_GUziBdtx', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-16 16:41:48', '2020-08-16 16:41:48'),
(12, 'Marc-Aurèle Yapo', 'marcaureleyapo@gmail.com', '$2y$10$eJEUh47Mve7yrjWYI8Qa5u3OSxah0g4n1TXMF5/BLs9Sy4J1eQK4i', '0urmSbcdamgabOgo69I5ldrJSbD6Aie9hKunI2aC7LZt7XIIFRGj2kWON1gH', 'cV566cgVRiaJUz7Rw9Z74g:APA91bF1BhRzeaC6UTaNqZhiZeo77T0qefqPeJ0lgYVh-T7wjlO2ooOpx4lbdVUH1EgdZ__GOsBmk9fDWfN3p8Y5ie8VjBCml5h62we3sVcxwXWOEwaRwa8w9bngJAzy_mzTPvD2A98s', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-26 13:49:18', '2020-08-26 13:49:18');

-- --------------------------------------------------------

--
-- Structure de la table `user_markets`
--

CREATE TABLE `user_markets` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `market_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user_markets`
--

INSERT INTO `user_markets` (`user_id`, `market_id`) VALUES
(2, 3),
(2, 4),
(2, 11);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `app_settings`
--
ALTER TABLE `app_settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `app_settings_key_index` (`key`);

--
-- Index pour la table `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `carts_product_id_foreign` (`product_id`),
  ADD KEY `carts_user_id_foreign` (`user_id`);

--
-- Index pour la table `cart_options`
--
ALTER TABLE `cart_options`
  ADD PRIMARY KEY (`option_id`,`cart_id`),
  ADD KEY `cart_options_cart_id_foreign` (`cart_id`);

--
-- Index pour la table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `cdg_commune`
--
ALTER TABLE `cdg_commune`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `cdg_subcategories`
--
ALTER TABLE `cdg_subcategories`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `cdg_zone`
--
ALTER TABLE `cdg_zone`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `currencies`
--
ALTER TABLE `currencies`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `custom_fields`
--
ALTER TABLE `custom_fields`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `custom_field_values`
--
ALTER TABLE `custom_field_values`
  ADD PRIMARY KEY (`id`),
  ADD KEY `custom_field_values_custom_field_id_foreign` (`custom_field_id`);

--
-- Index pour la table `delivery_addresses`
--
ALTER TABLE `delivery_addresses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `delivery_addresses_user_id_foreign` (`user_id`);

--
-- Index pour la table `drivers`
--
ALTER TABLE `drivers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `drivers_user_id_foreign` (`user_id`);

--
-- Index pour la table `drivers_payouts`
--
ALTER TABLE `drivers_payouts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `drivers_payouts_user_id_foreign` (`user_id`);

--
-- Index pour la table `driver_markets`
--
ALTER TABLE `driver_markets`
  ADD PRIMARY KEY (`user_id`,`market_id`),
  ADD KEY `driver_markets_market_id_foreign` (`market_id`);

--
-- Index pour la table `earnings`
--
ALTER TABLE `earnings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `earnings_market_id_foreign` (`market_id`);

--
-- Index pour la table `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `faqs_faq_category_id_foreign` (`faq_category_id`);

--
-- Index pour la table `faq_categories`
--
ALTER TABLE `faq_categories`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `favorites`
--
ALTER TABLE `favorites`
  ADD PRIMARY KEY (`id`),
  ADD KEY `favorites_product_id_foreign` (`product_id`),
  ADD KEY `favorites_user_id_foreign` (`user_id`);

--
-- Index pour la table `favorite_options`
--
ALTER TABLE `favorite_options`
  ADD PRIMARY KEY (`option_id`,`favorite_id`),
  ADD KEY `favorite_options_favorite_id_foreign` (`favorite_id`);

--
-- Index pour la table `fields`
--
ALTER TABLE `fields`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `galleries`
--
ALTER TABLE `galleries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `galleries_market_id_foreign` (`market_id`);

--
-- Index pour la table `markets`
--
ALTER TABLE `markets`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `markets_payouts`
--
ALTER TABLE `markets_payouts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `markets_payouts_market_id_foreign` (`market_id`);

--
-- Index pour la table `market_fields`
--
ALTER TABLE `market_fields`
  ADD PRIMARY KEY (`field_id`,`market_id`),
  ADD KEY `market_fields_market_id_foreign` (`market_id`);

--
-- Index pour la table `market_reviews`
--
ALTER TABLE `market_reviews`
  ADD PRIMARY KEY (`id`),
  ADD KEY `market_reviews_user_id_foreign` (`user_id`),
  ADD KEY `market_reviews_market_id_foreign` (`market_id`);

--
-- Index pour la table `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`id`),
  ADD KEY `media_model_type_model_id_index` (`model_type`,`model_id`);

--
-- Index pour la table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_type_model_id_index` (`model_type`,`model_id`);

--
-- Index pour la table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_type_model_id_index` (`model_type`,`model_id`);

--
-- Index pour la table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_notifiable_type_notifiable_id_index` (`notifiable_type`,`notifiable_id`);

--
-- Index pour la table `options`
--
ALTER TABLE `options`
  ADD PRIMARY KEY (`id`),
  ADD KEY `options_product_id_foreign` (`product_id`),
  ADD KEY `options_option_group_id_foreign` (`option_group_id`);

--
-- Index pour la table `option_groups`
--
ALTER TABLE `option_groups`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_user_id_foreign` (`user_id`),
  ADD KEY `orders_order_status_id_foreign` (`order_status_id`),
  ADD KEY `orders_driver_id_foreign` (`driver_id`),
  ADD KEY `orders_delivery_address_id_foreign` (`delivery_address_id`),
  ADD KEY `orders_payment_id_foreign` (`payment_id`);

--
-- Index pour la table `order_statuses`
--
ALTER TABLE `order_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Index pour la table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `payments_user_id_foreign` (`user_id`);

--
-- Index pour la table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_market_id_foreign` (`market_id`),
  ADD KEY `products_category_id_foreign` (`category_id`);

--
-- Index pour la table `product_orders`
--
ALTER TABLE `product_orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_orders_product_id_foreign` (`product_id`),
  ADD KEY `product_orders_order_id_foreign` (`order_id`);

--
-- Index pour la table `product_order_options`
--
ALTER TABLE `product_order_options`
  ADD PRIMARY KEY (`product_order_id`,`option_id`),
  ADD KEY `product_order_options_option_id_foreign` (`option_id`);

--
-- Index pour la table `product_reviews`
--
ALTER TABLE `product_reviews`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_reviews_user_id_foreign` (`user_id`),
  ADD KEY `product_reviews_product_id_foreign` (`product_id`);

--
-- Index pour la table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Index pour la table `uploads`
--
ALTER TABLE `uploads`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_api_token_unique` (`api_token`);

--
-- Index pour la table `user_markets`
--
ALTER TABLE `user_markets`
  ADD PRIMARY KEY (`user_id`,`market_id`),
  ADD KEY `user_markets_market_id_foreign` (`market_id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `app_settings`
--
ALTER TABLE `app_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=134;

--
-- AUTO_INCREMENT pour la table `carts`
--
ALTER TABLE `carts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pour la table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `cdg_commune`
--
ALTER TABLE `cdg_commune`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `cdg_subcategories`
--
ALTER TABLE `cdg_subcategories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `cdg_zone`
--
ALTER TABLE `cdg_zone`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT pour la table `currencies`
--
ALTER TABLE `currencies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pour la table `custom_fields`
--
ALTER TABLE `custom_fields`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `custom_field_values`
--
ALTER TABLE `custom_field_values`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT pour la table `delivery_addresses`
--
ALTER TABLE `delivery_addresses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT pour la table `drivers`
--
ALTER TABLE `drivers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `drivers_payouts`
--
ALTER TABLE `drivers_payouts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `earnings`
--
ALTER TABLE `earnings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `faqs`
--
ALTER TABLE `faqs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT pour la table `faq_categories`
--
ALTER TABLE `faq_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `favorites`
--
ALTER TABLE `favorites`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT pour la table `fields`
--
ALTER TABLE `fields`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `galleries`
--
ALTER TABLE `galleries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT pour la table `markets`
--
ALTER TABLE `markets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT pour la table `markets_payouts`
--
ALTER TABLE `markets_payouts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `market_reviews`
--
ALTER TABLE `market_reviews`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT pour la table `media`
--
ALTER TABLE `media`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;

--
-- AUTO_INCREMENT pour la table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT pour la table `options`
--
ALTER TABLE `options`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT pour la table `option_groups`
--
ALTER TABLE `option_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `order_statuses`
--
ALTER TABLE `order_statuses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=176;

--
-- AUTO_INCREMENT pour la table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `product_orders`
--
ALTER TABLE `product_orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `product_reviews`
--
ALTER TABLE `product_reviews`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT pour la table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `uploads`
--
ALTER TABLE `uploads`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `carts`
--
ALTER TABLE `carts`
  ADD CONSTRAINT `carts_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `carts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `cart_options`
--
ALTER TABLE `cart_options`
  ADD CONSTRAINT `cart_options_cart_id_foreign` FOREIGN KEY (`cart_id`) REFERENCES `carts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `cart_options_option_id_foreign` FOREIGN KEY (`option_id`) REFERENCES `options` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `custom_field_values`
--
ALTER TABLE `custom_field_values`
  ADD CONSTRAINT `custom_field_values_custom_field_id_foreign` FOREIGN KEY (`custom_field_id`) REFERENCES `custom_fields` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `delivery_addresses`
--
ALTER TABLE `delivery_addresses`
  ADD CONSTRAINT `delivery_addresses_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `drivers`
--
ALTER TABLE `drivers`
  ADD CONSTRAINT `drivers_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `drivers_payouts`
--
ALTER TABLE `drivers_payouts`
  ADD CONSTRAINT `drivers_payouts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `driver_markets`
--
ALTER TABLE `driver_markets`
  ADD CONSTRAINT `driver_markets_market_id_foreign` FOREIGN KEY (`market_id`) REFERENCES `markets` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `driver_markets_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `earnings`
--
ALTER TABLE `earnings`
  ADD CONSTRAINT `earnings_market_id_foreign` FOREIGN KEY (`market_id`) REFERENCES `markets` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `faqs`
--
ALTER TABLE `faqs`
  ADD CONSTRAINT `faqs_faq_category_id_foreign` FOREIGN KEY (`faq_category_id`) REFERENCES `faq_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `favorites`
--
ALTER TABLE `favorites`
  ADD CONSTRAINT `favorites_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `favorites_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `favorite_options`
--
ALTER TABLE `favorite_options`
  ADD CONSTRAINT `favorite_options_favorite_id_foreign` FOREIGN KEY (`favorite_id`) REFERENCES `favorites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `favorite_options_option_id_foreign` FOREIGN KEY (`option_id`) REFERENCES `options` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `galleries`
--
ALTER TABLE `galleries`
  ADD CONSTRAINT `galleries_market_id_foreign` FOREIGN KEY (`market_id`) REFERENCES `markets` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `markets_payouts`
--
ALTER TABLE `markets_payouts`
  ADD CONSTRAINT `markets_payouts_market_id_foreign` FOREIGN KEY (`market_id`) REFERENCES `markets` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `market_fields`
--
ALTER TABLE `market_fields`
  ADD CONSTRAINT `market_fields_field_id_foreign` FOREIGN KEY (`field_id`) REFERENCES `fields` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `market_fields_market_id_foreign` FOREIGN KEY (`market_id`) REFERENCES `markets` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `market_reviews`
--
ALTER TABLE `market_reviews`
  ADD CONSTRAINT `market_reviews_market_id_foreign` FOREIGN KEY (`market_id`) REFERENCES `markets` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `market_reviews_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `options`
--
ALTER TABLE `options`
  ADD CONSTRAINT `options_option_group_id_foreign` FOREIGN KEY (`option_group_id`) REFERENCES `option_groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `options_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_delivery_address_id_foreign` FOREIGN KEY (`delivery_address_id`) REFERENCES `delivery_addresses` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `orders_driver_id_foreign` FOREIGN KEY (`driver_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `orders_order_status_id_foreign` FOREIGN KEY (`order_status_id`) REFERENCES `order_statuses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `orders_payment_id_foreign` FOREIGN KEY (`payment_id`) REFERENCES `payments` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `orders_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `payments`
--
ALTER TABLE `payments`
  ADD CONSTRAINT `payments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `cdg_subcategories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `products_market_id_foreign` FOREIGN KEY (`market_id`) REFERENCES `markets` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `product_orders`
--
ALTER TABLE `product_orders`
  ADD CONSTRAINT `product_orders_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_orders_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `product_order_options`
--
ALTER TABLE `product_order_options`
  ADD CONSTRAINT `product_order_options_option_id_foreign` FOREIGN KEY (`option_id`) REFERENCES `options` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_order_options_product_order_id_foreign` FOREIGN KEY (`product_order_id`) REFERENCES `product_orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `product_reviews`
--
ALTER TABLE `product_reviews`
  ADD CONSTRAINT `product_reviews_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_reviews_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `user_markets`
--
ALTER TABLE `user_markets`
  ADD CONSTRAINT `user_markets_market_id_foreign` FOREIGN KEY (`market_id`) REFERENCES `markets` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_markets_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
