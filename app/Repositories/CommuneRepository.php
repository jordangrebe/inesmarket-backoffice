<?php

namespace App\Repositories;

use App\Models\Commune;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class FaqRepository
 * @package App\Repositories
 * @version August 29, 2019, 9:39 pm UTC
 *
 * @method Commune findWithoutFail($id, $columns = ['*'])
 * @method Commune find($id, $columns = ['*'])
 * @method Commune first($columns = ['*'])
*/
class CommuneRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'commune_name',
        'ZoneID'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Commune::class;
    }
}
