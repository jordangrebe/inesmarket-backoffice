<?php

namespace App\Http\Controllers;

use App\DataTables\ZoneDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateZoneRequest;
use App\Http\Requests\UpdateZoneRequest;
use App\Models\Zone;
use App\Repositories\ZoneRepository;
use App\Repositories\CustomFieldRepository;

use Flash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Prettus\Validator\Exceptions\ValidatorException;

class ZoneController extends Controller
{
    /** @var  ZoneRepository */
    private $zoneRepository;

    /**
     * @var CustomFieldRepository
     */
    private $customFieldRepository;

    

    public function __construct(ZoneRepository $zoneRepo, CustomFieldRepository $customFieldRepo )
    {
        parent::__construct();
        $this->zoneRepository = $zoneRepo;
        $this->customFieldRepository = $customFieldRepo;

    }

    /**
     * Display a listing of the FaqCategory.
     *
     * @param ZoneDataTable $zoneDataTable
     * @return Response
     */
    public function index(ZoneDataTable $zoneDataTable)
    {
        //$data =  Zone::all();
        return $zoneDataTable->render('zone.index');
    }

    /**
     * Show the form for creating a new FaqCategory.
     *
     * @return Response
     */
    public function create()
    {
        
        
        $hasCustomField = in_array($this->zoneRepository->model(),setting('custom_field_models',[]));
            if($hasCustomField){
                $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->zoneRepository->model());
                $html = generateCustomField($customFields);
            }
        return view('zone.create')->with("customFields", isset($html) ? $html : false);
    }

    /**
     * Store a newly created FaqCategory in storage.
     *
     * @param CreateZoneRequest $request
     *
     * @return Response
     */
    public function store(CreateZoneRequest $request)
    {

        $input = $request->all();
        $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->zoneRepository->model());
        try {
            $zone = $this->zoneRepository->create($input);
            $zone->customFieldsValues()->createMany(getCustomFieldsValues($customFields,$request));
            
        } catch (ValidatorException $e) {
            Flash::error($e->getMessage());
        }

        Flash::success(__('lang.saved_successfully',['operator' => __('lang.faq_category')]));

        return redirect(route('zone.index'));
    }

    /**
     * Display the specified FaqCategory.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $zone = $this->zoneRepository->findWithoutFail($id);

        if (empty($zone)) {
            Flash::error('Faq Category not found');

            return redirect(route('zone.index'));
        }

        return view('zone.show')->with('zone', $zone);
    }

    /**
     * Show the form for editing the specified FaqCategory.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        //= Zone::where(["zone_id" => $id]);
        $zone = $this->zoneRepository->findWithoutFail($id);


        if (empty($zone)) {
            Flash::error(__('lang.not_found',['operator' => __('lang.faq_category')]));

            return redirect(route('zone.index'));
        }
        $customFieldsValues = $zone->customFieldsValues()->with('customField')->get();
        $customFields =  $this->customFieldRepository->findByField('custom_field_model', $this->zoneRepository->model());
        $hasCustomField = in_array($this->zoneRepository->model(),setting('custom_field_models',[]));
        if($hasCustomField) {
            $html = generateCustomField($customFields, $customFieldsValues);
        }

        return view('zone.edit')->with('zone', $zone)->with("customFields", isset($html) ? $html : false);
    }

    /**
     * Update the specified FaqCategory in storage.
     *
     * @param  int              $id
     * @param UpdateZoneRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateZoneRequest $request)
    {
        $zone = $this->zoneRepository->findWithoutFail($id);

        if (empty($zone)) {
            Flash::error('Faq Category not found');
            return redirect(route('zone.index'));
        }
        $input = $request->all();
        $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->zoneRepository->model());
        try {
            $zone = $this->zoneRepository->update($input, $id);
            
            
            foreach (getCustomFieldsValues($customFields, $request) as $value){
                $zone->customFieldsValues()
                    ->updateOrCreate(['custom_field_id'=>$value['custom_field_id']],$value);
            }
        } catch (ValidatorException $e) {
            Flash::error($e->getMessage());
        }

        Flash::success(__('lang.updated_successfully',['operator' => __('lang.faq_category')]));

        return redirect(route('zone.index'));
    }

    /**
     * Remove the specified FaqCategory from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $zone = $this->zoneRepository->findWithoutFail($id);
        if (empty($zone)) {
            Flash::error('Zone introuvable');

            return redirect(route('zone.index'));
        }

        $zone->delete();

        Flash::success(__('lang.deleted_successfully',['operator' => __('lang.faq_category')]));

        return redirect(route('zone.index'));
    }

        /**
     * Remove Media of FaqCategory
     * @param Request $request
     */
    public function removeMedia(Request $request)
    {
        $input = $request->all();
        $zone = $this->ZoneRepository->findWithoutFail($input['id']);
        try {
            if($zone->hasMedia($input['collection'])){
                $zone->getFirstMedia($input['collection'])->delete();
            }
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }
    }
}
