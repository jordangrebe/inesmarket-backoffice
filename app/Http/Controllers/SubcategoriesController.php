<?php

namespace App\Http\Controllers;

use App\DataTables\SubcategoriesDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateSubcategoriesRequest;
use App\Http\Requests\UpdateSubcategoriesRequest;
use App\Repositories\SubcategoriesRepository;
use App\Repositories\CustomFieldRepository;
use App\Repositories\CategoryRepository;
use Flash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Prettus\Validator\Exceptions\ValidatorException;

class SubcategoriesController extends Controller
{
    /** @var  FaqRepository */
    private $subcategoriesRepository;

    /**
     * @var CustomFieldRepository
     */
    private $customFieldRepository;

    /**
     * @var FaqCategoryRepository
     */
    private $categoryRepository;

    public function __construct(SubcategoriesRepository $subcategoriesRepo, CustomFieldRepository $customFieldRepo , CategoryRepository $categoryRepo)
    {
        parent::__construct();
        $this->subcategoriesRepository = $subcategoriesRepo;
        $this->customFieldRepository = $customFieldRepo;
        $this->categoryRepository = $categoryRepo;
    }

    /**
     * Display a listing of the Faq.
     *
     * @param FaqDataTable $faqDataTable
     * @return Response
     */
    public function index(SubcategoriesDataTable $subcategoriesDataTable)
    {
        return $subcategoriesDataTable->render('subcategories.index');
    }

    /**
     * Show the form for creating a new Faq.
     *
     * @return Response
     */
    public function create()
    {
        $category = $this->categoryRepository->pluck('name','id');

        $hasCustomField = in_array($this->subcategoriesRepository->model(),setting('custom_field_models',[]));
        if($hasCustomField){
            $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->subcategoriesRepository->model());
            $html = generateCustomField($customFields);
        }
        return view('subcategories.create')->with("customFields", isset($html) ? $html : false)->with("category",$category);
    }

    /**
     * Store a newly created Faq in storage.
     *
     * @param CreateFaqRequest $request
     *
     * @return Response
     */
    public function store(CreateSubcategoriesRequest $request)
    {
        $input = $request->all();
        $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->subcategoriesRepository->model());
        try {
            $subcategories = $this->subcategoriesRepository->create($input);
            $subcategories->customFieldsValues()->createMany(getCustomFieldsValues($customFields,$request));

        } catch (ValidatorException $e) {
            Flash::error($e->getMessage());
        }

        Flash::success(__('lang.saved_successfully',['operator' => __('lang.faq')]));

        return redirect(route('subcategories.index'));
    }

    /**
     * Display the specified Faq.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $subcategories = $this->subcategoriesRepository->findWithoutFail($id);

        if (empty($subcategories)) {
            Flash::error('Faq not found');

            return redirect(route('subcategories.index'));
        }

        return view('subcategories.show')->with('subcategories', $subcategories);
    }

    /**
     * Show the form for editing the specified Faq.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $subcategories = $this->subcategoriesRepository->findWithoutFail($id);
        $category = $this->categoryRepository->pluck('name','id');


        if (empty($subcategories)) {
            Flash::error(__('lang.not_found',['operator' => __('lang.faq')]));

            return redirect(route('subcategories.index'));
        }
        $customFieldsValues = $subcategories->customFieldsValues()->with('customField')->get();
        $customFields =  $this->customFieldRepository->findByField('custom_field_model', $this->subcategoriesRepository->model());
        $hasCustomField = in_array($this->subcategoriesRepository->model(),setting('custom_field_models',[]));
        if($hasCustomField) {
            $html = generateCustomField($customFields, $customFieldsValues);
        }

        return view('subcategories.edit')->with('subcategories', $subcategories)->with("customFields", isset($html) ? $html : false)->with("category",$category);
    }

    /**
     * Update the specified Faq in storage.
     *
     * @param  int              $id
     * @param UpdateFaqRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSubcategoriesRequest $request)
    {
        $subcategories = $this->subcategoriesRepository->findWithoutFail($id);

        if (empty($subcategories)) {
            Flash::error('Faq not found');
            return redirect(route('subcategories.index'));
        }
        $input = $request->all();
        $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->subcategoriesRepository->model());
        try {
            $subcategories = $this->subcategoriesRepository->update($input, $id);


            foreach (getCustomFieldsValues($customFields, $request) as $value){
                $subcategories->customFieldsValues()
                    ->updateOrCreate(['custom_field_id'=>$value['custom_field_id']],$value);
            }
        } catch (ValidatorException $e) {
            Flash::error($e->getMessage());
        }

        Flash::success(__('lang.updated_successfully',['operator' => __('lang.faq')]));

        return redirect(route('subcategories.index'));
    }

    /**
     * Remove the specified Faq from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $subcategories = $this->subcategoriesRepository->findWithoutFail($id);

        if (empty($subcategories)) {
            Flash::error('Faq not found');

            return redirect(route('subcategories.index'));
        }

        $this->subcategoriesRepository->delete($id);

        Flash::success(__('lang.deleted_successfully',['operator' => __('lang.faq')]));

        return redirect(route('subcategories.index'));
    }

    /**
     * Remove Media of Faq
     * @param Request $request
     */
    public function removeMedia(Request $request)
    {
        $input = $request->all();
        $faq = $this->faqRepository->findWithoutFail($input['id']);
        try {
            if($faq->hasMedia($input['collection'])){
                $faq->getFirstMedia($input['collection'])->delete();
            }
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }
    }
}
