<?php

namespace App\Http\Controllers;

use App\DataTables\CommuneDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateCommuneRequest;
use App\Http\Requests\UpdateCommuneRequest;
use App\Models\Commune;
use App\Repositories\CommuneRepository;
use App\Repositories\CustomFieldRepository;
use App\Repositories\ZoneRepository;
use Flash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Prettus\Validator\Exceptions\ValidatorException;

class CommuneController extends Controller
{
    /** @var  CommuneRepository */
    private $communeRepository;

    /**
     * @var CustomFieldRepository
     */
    private $customFieldRepository;

    /**
  * @var ZoneRepository
  */
private $zoneRepository;

    public function __construct(CommuneRepository $communeRepo, CustomFieldRepository $customFieldRepo , zoneRepository $zoneRepo)
    {
        parent::__construct();
        $this->communeRepository = $communeRepo;
        $this->customFieldRepository = $customFieldRepo;
        $this->zoneRepository = $zoneRepo;
    }

    /**
     * Display a listing of the Faq.
     *
     * @param CommuneDataTable $communeDataTable
     * @return Response
     */
    public function index(CommuneDataTable $communeDataTable)
    {
        return $communeDataTable->render('commune.index');
    }

    /**
     * Show the form for creating a new Faq.
     *
     * @return Response
     */
    public function create()
    {
        $zone = $this->zoneRepository->pluck('zone_name','id');
        
        $hasCustomField = in_array($this->communeRepository->model(),setting('custom_field_models',[]));
            if($hasCustomField){
                $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->communeRepository->model());
                $html = generateCustomField($customFields);
            }
        return view('commune.create')->with("customFields", isset($html) ? $html : false)->with("zone",$zone);
    }

    /**
     * Store a newly created Faq in storage.
     *
     * @param CreateCommuneRequest $request
     *
     * @return Response
     */
    public function store(CreateCommuneRequest $request)
    {
        $input = $request->all();
        $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->communeRepository->model());
        try {
            $commune = $this->communeRepository->create($input);
            $commune->customFieldsValues()->createMany(getCustomFieldsValues($customFields,$request));
            
        } catch (ValidatorException $e) {
            Flash::error($e->getMessage());
        }

        Flash::success(__('lang.saved_successfully',['operator' => __('lang.faq')]));

        return redirect(route('commune.index'));
    }

    /**
     * Display the specified Faq.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $commune = $this->communeRepository->findWithoutFail($id);

        if (empty($commune)) {
            Flash::error('Faq not found');

            return redirect(route('commune.index'));
        }

        return view('commune.show')->with('commune', $commune);
    }

    /**
     * Show the form for editing the specified Faq.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $commune = $this->communeRepository->findWithoutFail($id);
        $zone = $this->zoneRepository->pluck('zone_name','id');
        

        if (empty($commune)) {
            Flash::error(__('lang.not_found',['operator' => __('lang.faq')]));

            return redirect(route('commune.index'));
        }
        $customFieldsValues = $commune->customFieldsValues()->with('customField')->get();
        $customFields =  $this->customFieldRepository->findByField('custom_field_model', $this->communeRepository->model());
        $hasCustomField = in_array($this->communeRepository->model(),setting('custom_field_models',[]));
        if($hasCustomField) {
            $html = generateCustomField($customFields, $customFieldsValues);
        }

        return view('commune.edit')->with('commune', $commune)->with("customFields", isset($html) ? $html : false)->with("zone",$zone);
    }

    /**
     * Update the specified Faq in storage.
     *
     * @param  int              $id
     * @param UpdateCommuneRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCommuneRequest $request)
    {
        $commune = $this->communeRepository->findWithoutFail($id);

        if (empty($commune)) {
            Flash::error('Faq not found');
            return redirect(route('commune.index'));
        }
        $input = $request->all();
        $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->communeRepository->model());
        try {
            $commune = $this->communeRepository->update($input, $id);
            
            
            foreach (getCustomFieldsValues($customFields, $request) as $value){
                $commune->customFieldsValues()
                    ->updateOrCreate(['custom_field_id'=>$value['custom_field_id']],$value);
            }
        } catch (ValidatorException $e) {
            Flash::error($e->getMessage());
        }

        Flash::success(__('lang.updated_successfully',['operator' => __('lang.faq')]));

        return redirect(route('commune.index'));
    }

    /**
     * Remove the specified Faq from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $commune = $this->communeRepository->findWithoutFail($id);

        if (empty($commune)) {
            Flash::error('Faq not found');

            return redirect(route('commune.index'));
        }

        $this->communeRepository->delete($id);

        Flash::success(__('lang.deleted_successfully',['operator' => __('lang.faq')]));

        return redirect(route('commune.index'));
    }

        /**
     * Remove Media of Faq
     * @param Request $request
     */
    public function removeMedia(Request $request)
    {
        $input = $request->all();
        $commune = $this->communeRepository->findWithoutFail($input['id']);
        try {
            if($commune->hasMedia($input['collection'])){
                $commune->getFirstMedia($input['collection'])->delete();
            }
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }
    }
}
