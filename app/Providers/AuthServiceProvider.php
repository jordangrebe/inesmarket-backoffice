<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //route of zone
        Gate::define('zone.index', function ($user) {
            return true;
        });

        Gate::define('zone.create', function ($user) {
            return true;
        });

        /*Gate::define('zone.show', function ($user) {
            return true;
        });*/

        Gate::define('zone.edit', function ($user) {
            return true;
        });

        Gate::define('zone.destroy', function ($user) {
            return true;
        });

        Gate::define('zone.store', function ($user) {
            return true;
        });

        Gate::define('zone.update', function ($user) {
            return true;
        });

        //route of commune
        Gate::define('commune.index', function ($user) {
            return true;
        });

        Gate::define('commune.create', function ($user) {
            return true;
        });

        /*Gate::define('zone.show', function ($user) {
            return true;
        });*/

        Gate::define('commune.edit', function ($user) {
            return true;
        });

        Gate::define('commune.destroy', function ($user) {
            return true;
        });

        Gate::define('commune.store', function ($user) {
            return true;
        });

        Gate::define('commune.update', function ($user) {
            return true;
        });

        //route of commune
        Gate::define('subcategories.index', function ($user) {
            return true;
        });

        Gate::define('subcategories.create', function ($user) {
            return true;
        });

        /*Gate::define('zone.show', function ($user) {
            return true;
        });*/

        Gate::define('subcategories.edit', function ($user) {
            return true;
        });

        Gate::define('subcategories.destroy', function ($user) {
            return true;
        });

        Gate::define('subcategories.store', function ($user) {
            return true;
        });

        Gate::define('subcategories.update', function ($user) {
            return true;
        });

    }
}
