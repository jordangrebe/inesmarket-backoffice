<?php
/*
|--------------------------------------------------------------------------
| Coddyger - PHP Environment
|--------------------------------------------------------------------------
|
| Authors :: UltronDev
| Website :: https://www.ultrondev.com/
|
*/
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST, GET, PUT");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require __DIR__ . '/vendor/autoload.php';

$dotenv = Dotenv\Dotenv::create(__DIR__);
$dotenv->load();

class Api
{
    public function __construct()
    {
        $method = (isset($_GET['method'])) ? $_GET['method'] : '';
        $ref = (isset($_GET['ref'])) ? $_GET['ref'] : '';

        switch ($method){
            case 'zones':
                $model = self::getZones();
                self::api($model);
                break;
            case 'communes':
                $model = self::getCommunes();
                self::api($model);
                break;
            case 'commune/markets':
                $model = self::getMarketFromCommune($ref);
                self::api($model);
                break;
            case 'products/market':
                $model = self::getProductsFromMarket();
                self::api($model);
                break;
            case 'products/featured':
                $model = self::getFeaturedProducts();
                self::api($model);
                break;
            case 'categories/market':
                $model = self::getCategoriesFromMarket();
                self::api($model);
                break;
			case 'subcategories/market':
                $model = self::getSubcategoriesFromMarket($ref);
                self::api($model);
                break;
            default:
                $model = [
                    'response' => 'error',
                    'data' => 'Router json introuvable'
                ];
                self::api($model);
        }
    }
    public static function getZones() {
        $json = [];
        $temp = [];
        $i = 0;


        $Q = self::connect()->prepare('SELECT * FROM cdg_zone');
        $Q->execute();

        while ($data = $Q->fetch()){
            $data_ref = self::sanitize_output($data['id']);
            $data_name = self::sanitize_output($data['zone_name']);
            $data_reg = self::sanitize_output($data['created_at']);
            $data_update = self::sanitize_output($data['updated_at']);

            /*
            |------------------
            | OUTPUT ---------
            |------------------
            */
            $temp['data_id'] = (int)$i + 1;
            $temp['data_ref'] = (int)$data_ref;
            $temp['data_name'] = $data_name;
            $temp['data_reg'] = $data_reg;
            $temp['data_update'] = $data_update;

            $json[$i] = $temp;
            $i++;
        }

        return self::buildApiResponse($json, 'success');
    }
    public static function getCommunes() {
        $json = [];
        $temp = [];
        $i = 0;

        $db = self::connect();
        $db->query("set names utf8");


        $Q = $db->prepare('SELECT cdg_commune.id, cdg_commune.commune_name, cdg_commune.updated_at, cdg_commune.created_at, zoneID, zone.name AS zoneName FROM cdg_commune LEFT JOIN cdg_zone on cdg_zone.id = zoneID');
        $Q->execute();

        while ($data = $Q->fetch()){
            $data_ref = self::sanitize_output($data['id']);
            $data_name = self::sanitize_output($data['name']);
            $data_reg = self::sanitize_output($data['created_at']);
            $data_update = self::sanitize_output($data['updated_at']);
            $zoneID = self::sanitize_output($data['zoneID']);
            $zoneName = self::sanitize_output($data['zoneName']);

            /*
            |------------------
            | OUTPUT ---------
            |------------------
            */
            $temp['data_id'] = (int)$i + 1;
            $temp['data_ref'] = (int)$data_ref;
            $temp['data_name'] = $data_name;
            $temp['data_reg'] = $data_reg;
            $temp['data_update'] = $data_update;
            $temp['zoneID'] = (int)$zoneID;
            $temp['zoneName'] = $zoneName;

            $json[$i] = $temp;
            $i++;
        }

        return self::buildApiResponse($json, 'success');
    }
    public static function getMarketFromCommune($communeID) {
        $json = [];
        $temp = [];
        $i = 0;

        $db = self::connect();
        $db->query("set names utf8");


        $Q = $db->prepare('SELECT *, markets.created_at AS marketReg, markets.updated_at AS marketUpdate FROM markets LEFT JOIN cdg_commune ON cdg_commune.id = markets.id WHERE communeID = :communeID');
        $Q->bindValue(':communeID', $communeID, PDO::PARAM_INT);
        $Q->execute();

        while ($data = $Q->fetch()){
            $id = self::sanitize_output($data['id']);
            $name = self::sanitize_output($data['name']);
            $description = self::sanitize_output($data['description']);
            $address = self::sanitize_output($data['address']);
            $latitude = self::sanitize_output($data['latitude']);
            $longitude = self::sanitize_output($data['longitude']);
            $phone = self::sanitize_output($data['phone']);
            $mobile = self::sanitize_output($data['mobile']);
            $information = self::sanitize_output($data['information']);
            $admin_commission = self::sanitize_output($data['admin_commission']);
            $delivery_fee = self::sanitize_output($data['delivery_fee']);
            $delivery_range = self::sanitize_output($data['delivery_range']);
            $default_tax = self::sanitize_output($data['default_tax']);
            $closed = (int)self::sanitize_output($data['closed']);
            $available_for_delivery = (int)self::sanitize_output($data['available_for_delivery']);
            $created_at = self::sanitize_output($data['marketReg']);
            $updated_at = self::sanitize_output($data['marketUpdate']);
            $custom_fields = [];
            $media = self::getMedia($id);
            $hasMedia = (count($media) >= 1 || $media !== null ? true : false);
            /*
            |------------------
            | OUTPUT ---------
            |------------------
            */
            $temp['id'] = (int)$id;
            $temp['name'] = $name;
            $temp['description'] = $description;
            $temp['address'] = $address;
            $temp['latitude'] = $latitude;
            $temp['longitude'] = $longitude;
            $temp['phone'] = $phone;
            $temp['mobile'] = $mobile;
            $temp['information'] = $information;
            $temp['admin_commission'] = (double)$admin_commission;
            $temp['delivery_fee'] = (double)$delivery_fee;
            $temp['delivery_range'] = (double)$delivery_range;
            $temp['default_tax'] = (double)$default_tax;
            $temp['closed'] = ($closed === 1 ? true : false);
            $temp['available_for_delivery'] = ($available_for_delivery === 1 ? true : false);
            $temp['created_at'] = $created_at;
            $temp['updated_at'] = $updated_at;
            $temp['custom_fields'] = $custom_fields;
            $temp['has_media'] = $hasMedia;
            $temp['media'] = $media;

            $json[$i] = $temp;
            $i++;
        }

        return self::buildApiResponse($json, 'success');
    }
    public static function getProductsFromMarket() {
        $json = [];
        $temp = [];
        $i = 0;

        $db = self::connect();
        $db->query("set names utf8");
        $ref = (isset($_GET['ref'])) ? $_GET['ref'] : '';

        $Q = $db->prepare('SELECT * FROM products WHERE market_id = :marketID ORDER BY created_at DESC');
        $Q->bindValue(':marketID', $ref, PDO::PARAM_INT);
        $Q->execute();

        while ($data = $Q->fetch()){
            $id = (int)self::sanitize_output($data['id']);
            $name = self::sanitize_output($data['name']);
            $price = (double)self::sanitize_output($data['price']);
            $discount_price = (double)self::sanitize_output($data['discount_price']);
            $description = self::sanitize_output($data['description']);
            $capacity = (double)self::sanitize_output($data['capacity']);
            $package_items_count = (double)self::sanitize_output($data['package_items_count']);
            $unit = self::sanitize_output($data['unit']);
            $featured = (int)self::sanitize_output($data['featured']); // Bool
            $deliverable = (int)self::sanitize_output($data['deliverable']); // Bool
            $market_id = (int)self::sanitize_output($data['market_id']);
            $category_id = (int)self::sanitize_output($data['category_id']);
            $created_at = self::sanitize_output($data['created_at']);
            $updated_at = self::sanitize_output($data['updated_at']);
            $custom_fields = [];
            $market = self::getMarket($market_id);
            $media = self::getMedia($id);
            $has_media = (count($media) >= 1 || $media !== null ? true : false); // Bool
            /*
            |------------------
            | OUTPUT ---------
            |------------------
            */
            $temp['id'] = $id;
            $temp['name'] = $name;
            $temp['price'] = $price;
            $temp['discount_price'] = $discount_price;
            $temp['description'] = $description;
            $temp['capacity'] = $capacity;
            $temp['package_items_count'] = $package_items_count;
            $temp['unit'] = $unit;
            $temp['featured'] = ($featured === 1 ? true : false);
            $temp['deliverable'] = ($deliverable === 1 ? true : false);
            $temp['market_id'] = $market_id;
            $temp['category_id'] = $category_id;
            $temp['created_at'] = $created_at;
            $temp['updated_at'] = $updated_at;
            $temp['custom_fields'] = $custom_fields;
            $temp['has_media'] = $has_media;
            $temp['market'] = null;
            $temp['media'] = self::getMedia($id);

            $json[$i] = $temp;
            $i++;
        }

        return self::buildApiResponse($json, 'success');
    }
    public static function getFeaturedProducts() {
        $json = [];
        $temp = [];
        $i = 0;

        $db = self::connect();
        $db->query("set names utf8");
        $ref = (isset($_GET['ref'])) ? $_GET['ref'] : '';
        $featured = 1;

        $Q = $db->prepare('SELECT * FROM products WHERE (market_id = :a AND featured = :b) AND (YEARWEEK(`created_at`, 1) = YEARWEEK(CURDATE(), 1) OR YEARWEEK(`updated_at`, 1) = YEARWEEK(CURDATE(), 1)) ORDER BY RAND() LIMIT 15');
        $Q->bindValue(':a', $ref, PDO::PARAM_INT);
        $Q->bindValue(':b', $featured, PDO::PARAM_INT);
        $Q->execute();

        while ($data = $Q->fetch()){
            $id = (int)self::sanitize_output($data['id']);
            $name = self::sanitize_output($data['name']);
            $price = (double)self::sanitize_output($data['price']);
            $discount_price = (double)self::sanitize_output($data['discount_price']);
            $description = self::sanitize_output($data['description']);
            $capacity = (double)self::sanitize_output($data['capacity']);
            $package_items_count = (double)self::sanitize_output($data['package_items_count']);
            $unit = self::sanitize_output($data['unit']);
            $featured = (int)self::sanitize_output($data['featured']); // Bool
            $deliverable = (int)self::sanitize_output($data['deliverable']); // Bool
            $market_id = (int)self::sanitize_output($data['market_id']);
            $category_id = (int)self::sanitize_output($data['category_id']);
            $created_at = self::sanitize_output($data['created_at']);
            $updated_at = self::sanitize_output($data['updated_at']);
            $custom_fields = [];
            $market = self::getMarket($market_id);
            $media = self::getMedia($id);
            $has_media = (count($media) >= 1 || $media !== null ? true : false); // Bool
            /*
            |------------------
            | OUTPUT ---------
            |------------------
            */
            $temp['id'] = $id;
            $temp['name'] = $name;
            $temp['price'] = $price;
            $temp['discount_price'] = $discount_price;
            $temp['description'] = $description;
            $temp['capacity'] = $capacity;
            $temp['package_items_count'] = $package_items_count;
            $temp['unit'] = $unit;
            $temp['featured'] = ($featured === 1 ? true : false);
            $temp['deliverable'] = ($deliverable === 1 ? true : false);
            $temp['market_id'] = $market_id;
            $temp['category_id'] = $category_id;
            $temp['created_at'] = $created_at;
            $temp['updated_at'] = $updated_at;
            $temp['custom_fields'] = $custom_fields;
            $temp['has_media'] = $has_media;
            $temp['market'] = null;
            $temp['media'] = self::getMedia($id);

            $json[$i] = $temp;
            $i++;
        }

        return self::buildApiResponse($json, 'success');
    }
    public static function getCategoriesFromMarket() {
        $json = [];
        $temp = [];
        $i = 0;

        $db = self::connect();
        $db->query("set names utf8");
        $ref = (isset($_GET['ref'])) ? $_GET['ref'] : '';

        $Q = $db->prepare('SELECT c.name as cName, c.id as cID FROM products LEFT JOIN categories c on products.category_id = c.id LEFT JOIN media ON model_id = c.id WHERE market_id = :marketID GROUP BY category_id');
        $Q->bindValue(':marketID', $ref, PDO::PARAM_INT);
        $Q->execute();

        while ($data = $Q->fetch()){
            $data_ref = self::sanitize_output($data['cID']);
            $data_name = self::sanitize_output($data['cName']);
            $media = self::getMedia($data_ref);
            /*
            |------------------
            | OUTPUT ---------
            |------------------
            */
            $temp['data_id'] = (int)$i + 1;
            $temp['ref'] = (int)$data_ref;
            $temp['name'] = $data_name;
            $temp['media'] = $media;

            $json[$i] = $temp;
            $i++;
        }

        return self::buildApiResponse($json, 'success');
    }
	public static function getSubcategoriesFromMarket() {
        $json = [];
        $temp = [];
        $i = 0;

        $db = self::connect();
        $db->query("set names utf8");
        $ref = (isset($_GET['ref'])) ? $_GET['ref'] : '';

        $Q = $db->prepare('SELECT c.name as cName, c.id as cID FROM products LEFT JOIN cdg_subcategories c on products.category_id = c.id LEFT JOIN media ON model_id = c.id WHERE market_id = :marketID GROUP BY category_id');
        $Q->bindValue(':marketID', $ref, PDO::PARAM_INT);
        $Q->execute();

        while ($data = $Q->fetch()){
            $data_ref = self::sanitize_output($data['cID']);
            $data_name = self::sanitize_output($data['cName']);
            $media = self::getMedia($data_ref);
            /*
            |------------------
            | OUTPUT ---------
            |------------------
            */
            $temp['data_id'] = (int)$i + 1;
            $temp['ref'] = (int)$data_ref;
            $temp['name'] = $data_name;
            $temp['media'] = $media;

            $json[$i] = $temp;
            $i++;
        }

        return self::buildApiResponse($json, 'success');
    }
    public static function connect(){
        $link = new PDO('mysql:host=' . getenv('DB_HOST') . ';dbname=' . getenv('DB_DATABASE'), getenv('DB_USERNAME'), getenv('DB_PASSWORD'), array(PDO::ERRMODE_EXCEPTION));
        return $link;
    }
    public static function api($model) {
        if(is_array($model)) {
            if(array_key_exists('response', $model) || array_key_exists('data', $model)){
                $response = $model['response'];
                $data = $model['data'];

                if($response == 'success'){
                    http_response_code(200);
                    self::apiJson(0, $response, $data);
                } else if($response == 'token'){
                    http_response_code(401);
                    self::apiJson(1, 'Unauthorized', $data);
                } else if($response == 'notfound'){
                    http_response_code(404);
                    self::apiJson(1, 'route not found', $data);
                } else {
                    http_response_code(500);
                    self::apiJson(1, 'error', $data);
                }
            } else {
                http_response_code(501);
                self::apiJson(2, 'critical error::required key not found', null);
            }
        } else {
            http_response_code(501);
            self::apiJson(2, 'critical error::parameter one needs to be array ' . gettype($model) . ' detected', null);
        }
    }
    public static function apiJson($status, $message, $data) {
        $json = array(
            'status' => $status,
            'message' => $message,
            'data' => $data
        );

        header('Content-Type: application/json');
        print json_encode($json);
    }
    public static function buildApiResponse($data, $response = 'error') {
        // -- NOTE ----------
        // Default response is "error"
        // For JWT Token error the response is "token"
        // For success alert the response is "success"
        return ['response' => $response, 'data' => $data];
    }
    public static function sanitize_output($string) {
        return htmlspecialchars($string);
    }
    private static function getMedia($modelID) {
        $json = [];
        $temp = [];
        $i = 0;

        $db = self::connect();
        $db->query("set names utf8");


        $Q = $db->prepare('SELECT * FROM media WHERE model_id = :modelID');
        $Q->bindValue(':modelID', $modelID, PDO::PARAM_INT);
        $Q->execute();

        while ($data = $Q->fetch()){
            $id = self::sanitize_output($data['id']);
            $model_type = self::sanitize_output($data['model_type']);
            $model_id = self::sanitize_output($data['model_id']);
            $collection_name = self::sanitize_output($data['collection_name']);
            $name = self::sanitize_output($data['name']);
            $file_name = self::sanitize_output($data['file_name']);
            $mime_type = self::sanitize_output($data['mime_type']);
            $disk = self::sanitize_output($data['disk']);
            $size = self::sanitize_output($data['size']);
            $manipulations = self::sanitize_output($data['manipulations']);
            $custom_properties = json_decode($data['custom_properties']);
            $responsive_images = self::sanitize_output($data['responsive_images']);
            $order_column = self::sanitize_output($data['order_column']);
            $created_at = self::sanitize_output($data['created_at']);
            $updated_at = self::sanitize_output($data['updated_at']);

            
            $url = self::buildMediaURI($id, $file_name);
            $thumb = self::buildMediaURI($id, $name, 'thumb');
            $icon = self::buildMediaURI($id, $name, 'icon');

            /*
            |------------------
            | OUTPUT ---------
            |------------------
            */
            $temp['id'] = (int)$id;
            $temp['model_type'] = $model_type;
            $temp['model_id'] = (int)$model_id;
            $temp['collection_name'] = $collection_name;
            $temp['name'] = $name;
            $temp['file_name'] = $file_name;
            $temp['mime_type'] = $mime_type;
            $temp['disk'] = $disk;
            $temp['size'] = (int)$size;
            $temp['manipulations'] = $manipulations;
            $temp['custom_properties'] = $custom_properties;
            $temp['responsive_images'] = $responsive_images;
            $temp['order_column'] = (int)$order_column;
            $temp['created_at'] = $created_at;
            $temp['updated_at'] = $updated_at;
            $temp['url'] = $url;
            $temp['thumb'] = $thumb;
            $temp['icon'] = $icon;

            $json[$i] = $temp;
            $i++;
        }

        return $json;
    }
    private static function getMarket($modelID) {
        $json = [];
        $temp = [];
        $i = 0;

        $db = self::connect();
        $db->query("set names utf8");


        $Q = $db->prepare('SELECT * FROM markets WHERE id = :modelID');
        $Q->bindValue(':modelID', $modelID, PDO::PARAM_INT);
        $Q->execute();

        while ($data = $Q->fetch()){
            $id = self::sanitize_output($data['id']);
            $name = self::sanitize_output($data['name']);
            $description = self::sanitize_output($data['description']);
            $address = self::sanitize_output($data['address']);
            $latitude = self::sanitize_output($data['latitude']);
            $longitude = self::sanitize_output($data['longitude']);
            $phone = self::sanitize_output($data['phone']);
            $mobile = self::sanitize_output($data['mobile']);
            $information = self::sanitize_output($data['information']);
            $admin_commission = self::sanitize_output($data['admin_commission']);
            $delivery_fee = self::sanitize_output($data['delivery_fee']);
            $delivery_range = self::sanitize_output($data['delivery_range']);
            $default_tax = self::sanitize_output($data['default_tax']);
            $closed = (int)self::sanitize_output($data['closed']);
            $available_for_delivery = (int)self::sanitize_output($data['available_for_delivery']);
            $created_at = self::sanitize_output($data['created_at']);
            $updated_at = self::sanitize_output($data['updated_at']);
            $custom_fields = [];
            $media = self::getMedia($id);
            $hasMedia = (count($media) >= 1 || $media !== null ? true : false);
            /*
            |------------------
            | OUTPUT ---------
            |------------------
            */
            $temp['id'] = (int)$id;
            $temp['name'] = $name;
            $temp['description'] = $description;
            $temp['address'] = $address;
            $temp['latitude'] = $latitude;
            $temp['longitude'] = $longitude;
            $temp['phone'] = $phone;
            $temp['mobile'] = $mobile;
            $temp['information'] = $information;
            $temp['admin_commission'] = (double)$admin_commission;
            $temp['delivery_fee'] = (double)$delivery_fee;
            $temp['delivery_range'] = (double)$delivery_range;
            $temp['default_tax'] = (double)$default_tax;
            $temp['closed'] = ($closed === 1 ? true : false);
            $temp['available_for_delivery'] = ($available_for_delivery === 1 ? true : false);
            $temp['created_at'] = $created_at;
            $temp['updated_at'] = $updated_at;
            $temp['custom_fields'] = $custom_fields;
            $temp['has_media'] = $hasMedia;
            $temp['media'] = $media;

            $json[$i] = $temp;
            $i++;
        }

        return $json;
    }
    private static function buildMediaURI($id, $filename, $type = '') {
		//print_r($_SERVER);
        if($type == 'thumb') {
            return $_SERVER['APP_URL'] . 'storage/app/public/' . $id . '/conversions/' . $filename . '-thumb.jpg';
        } elseif($type == 'icon') {
            return $_SERVER['APP_URL'] . 'storage/app/public/' . $id . '/conversions/' . $filename . '-icon.jpg';
        } else {
            return $_SERVER['APP_URL'] . 'storage/app/public/' . $id . '/' . $filename;
        }
    }
}

$data = new Api();
