@if($customFields)
<h5 class="col-12 pb-4">{!! trans('lang.main_fields') !!}</h5>
@endif
<div style="flex: 50%;max-width: 50%;padding: 0 4px;" class="column">
<!-- Question Field -->
<div class="form-group row ">
  {!! Form::label('name', trans("lang.subcategories_question"), ['class' => 'col-3 control-label text-right']) !!}
  <div class="col-9">
    {!! Form::text('name', null, ['class' => 'form-control','placeholder'=>
     trans("lang.subcategories_question_placeholder")  ]) !!}
    <div class="form-text text-muted">{{ trans("lang.subcategories_question_help") }}</div>
  </div>
</div>

<!-- Answer Field -->
<div class="form-group row ">
  {!! Form::label('description', trans("lang.subcategories_answer"), ['class' => 'col-3 control-label text-right']) !!}
  <div class="col-9">
    {!! Form::textarea('description', null, ['class' => 'form-control','placeholder'=>
     trans("lang.subcategories_placeholder")  ]) !!}
    <div class="form-text text-muted">{{ trans("lang.subcategories_answer_help") }}</div>
  </div>
</div>
</div>
<div style="flex: 50%;max-width: 50%;padding: 0 4px;" class="column">

<!-- Faq Category Id Field -->
<div class="form-group row ">
  {!! Form::label('IDcategorie', trans("lang.subcategories_category_id"),['class' => 'col-3 control-label text-right']) !!}
  <div class="col-9">
    {!! Form::select('IDcategorie', $category, null, ['class' => 'select2 form-control']) !!}
    <div class="form-text text-muted">{{ trans("lang.subcategories_category_id_help") }}</div>
  </div>
</div>

</div>
@if($customFields)
<div class="clearfix"></div>
<div class="col-12 custom-field-container">
  <h5 class="col-12 pb-4">{!! trans('lang.custom_field_plural') !!}</h5>
  {!! $customFields !!}
</div>
@endif
<!-- Submit Field -->
<div class="form-group col-12 text-right">
  <button type="submit" class="btn btn-{{setting('theme_color')}}" ><i class="fa fa-save"></i> {{trans('lang.save')}} {{trans('lang.subcategories')}}</button>
  <a href="{!! route('subcategories.index') !!}" class="btn btn-default"><i class="fa fa-undo"></i> {{trans('lang.cancel')}}</a>
</div>
