<!-- Id Field -->
<div class="form-group row col-6">
  {!! Form::label('id', 'Id:', ['class' => 'col-3 control-label text-right']) !!}
  <div class="col-9">
    <p>{!! $commune->id !!}</p>
  </div>
</div>

<!-- Question Field -->
<div class="form-group row col-6">
  {!! Form::label('question', 'Question:', ['class' => 'col-3 control-label text-right']) !!}
  <div class="col-9">
    <p>{!! $commune->commune_name !!}</p>
  </div>
</div>

<!-- Faq Category Id Field -->
<div class="form-group row col-6">
  {!! Form::label('faq_category_id', 'Faq Category Id:', ['class' => 'col-3 control-label text-right']) !!}
  <div class="col-9">
    <p>{!! $commune->commune_name !!}</p>
  </div>
</div>

<!-- Created At Field -->
<div class="form-group row col-6">
  {!! Form::label('created_at', 'Created At:', ['class' => 'col-3 control-label text-right']) !!}
  <div class="col-9">
    <p>{!! $commune->created_at !!}</p>
  </div>
</div>

<!-- Updated At Field -->
<div class="form-group row col-6">
  {!! Form::label('updated_at', 'Updated At:', ['class' => 'col-3 control-label text-right']) !!}
  <div class="col-9">
    <p>{!! $commune->updated_at !!}</p>
  </div>
</div>

