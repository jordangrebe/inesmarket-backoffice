<?php 

return [
    'back' => 'Retour',
    'back_to_home' => 'Retour au Dashboard',
    'page' => [
        'not_found' => 'Page introuvable',
    ],
    'permission' => 'Accès refusé',
    'store_disabled' => 'Magasin désactivé',
];